#!/bin/sh

#$ -N FunctionalAnnotationXXBREEDXX
#$ -cwd
#$ -o FunctionalAnnotation.out
#$ -e FunctionalAnnotation.err
#$ -l h_rt=4:00:00
#$ -R y
#$ -P roslin_hickey_group
#$ -pe sharedmem 1
#$ -l h_vmem=16G

# Initialise the environment modules
. /etc/profile.d/modules.sh

# Load
module load igmm/apps/R/3.3.0
module load roslin/plink/3.36-beta

# Standard report

echo "Working directory:"
pwd
date

## This command was added since the file from the last job are not available immediately
sleep 5


./FunctionalAnnotation.R


## Run Plink to get A or B coding information
export Breed="${PWD##*/}"

cd PotentiallyLethalHaplotypes || exit

ls | sed "s/\..*//" | uniq | sed -nr '/Chr|HaploLib|HaploID/p' > PotentiallyLethalHaplotypes.txt

while read i; do
  echo ${i}
  awk '{print $2}' ${i}.map > snps.txt
  plink --file ../../../GenotypesQualityCheck/${Breed}/AllChipsFinal --extract snps.txt --cow --recode A
  head -1 plink.raw > ${i}.raw
done < PotentiallyLethalHaplotypes.txt

cd ../ || exit

./LethalHaplotypeFormatChange.R


# Standard report
echo
pwd
date
