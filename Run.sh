#!/bin/bash

module load igmm/apps/R/3.3.0

## Here are commands for the preparation of genotypes for the ICBF data

qsub qsub_1_DataSummary.sh

sleep 10

./2_ExtractGenotypes.sh

sleep 10

./3_ExtractPedigree.sh

sleep 10

./4_PrepareChromosomes.sh

sleep 10

./5_RunAlphaPhase.sh 1

sleep 10

./5_RunAlphaPhase.sh 2

sleep 10

./5_RunAlphaPhase.sh 3

## When using segmentation option change AlphaPhaseSpec.txt file using the code below
## for i in `seq 1 1 29` ; do
##   cd Chromosome${i}/Phasing/ || exit
##   for j in `seq 1 1 20` ; do
##     cd Phase${j} || exit
##     head -n 15 AlphaPhaseSpec.txt > tmp.txt
##     echo "IterateMethod                     ,RandomOrder" >> tmp.txt
##     echo "IterateSubsetSize                 ,2000" >> tmp.txt
##     echo "IterateIterations                 ,1" >> tmp.txt
##     echo "Cores                             ,1,Combine" >> tmp.txt
##     echo "MinHapFreq                        ,1" >> tmp.txt
##     echo "Library                           ,None" >> tmp.txt
##     mv tmp.txt AlphaPhaseSpec.txt
##     cd .. || exit
##   done
##   cd ../../ || exit
## done

sleep 10

./5_RunAlphaPhase.sh 4

sleep 10

./5_RunAlphaPhase.sh 5

sleep 10

./5_RunAlphaPhase.sh 8

sleep 10

./6_HaplotypeStatistics.sh

sleep 10
## Run next line for AA and HE analysis using
./7_RunAlphaSeqOpt.sh

sleep 10

./8_Summary.sh

sleep 10

./9_RunAlphaSeqOptPedig.sh

sleep 10

./10_RunAlphaSeqOpt_Costs.sh

sleep 10

./11_SummaryAlphaSeqOptCosts.R

sleep 10

./12_PedigreeTracking.sh

sleep 10

./13_FunctionalAnnotation.sh
