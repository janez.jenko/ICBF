#!/bin/bash

module add roslin/plink/3.36-beta

export G=~/Hpc/Illumina/Janez/Genotypes
export A=~/Hpc/Illumina/Janez/Analysis_Purebreds_LmChAaSiHe
export S=~/Hpc/Illumina/Janez/Scripts

if [ ! -d ${A} ];
then
  mkdir ${A}
fi

if [ ! -d ${A}/GenotypesQualityCheck ];
then
  mkdir ${A}/GenotypesQualityCheck
fi

cd ${A}/GenotypesQualityCheck || exit

## -----------------------------------------------------------------------------
## Prepare file for extraction of the SNP that are present on both idbv3 and hd SNP chip
## and do a quality check on genotype call, level of heterozygosity and HW equilibirum
## The code for dealing with genotype data is in the file: qsub_3_ExtractGenotypes.sh

for i in ${A}/SelectedInd/*; do

  a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`
  echo ${a}

  if [ ! -d ${a} ]; then
    mkdir ${a}
  fi

  cd ${a} || return
  rm -f PrepGeno.err PrepGeno.out
  cp -rf ${S}/qsub_2_ExtractGenotypes.sh .
  sed -i "s/XXINDXX/${a}/g" qsub_2_ExtractGenotypes.sh
  sed -i "s/XXBREEDXX/${a}/g" qsub_2_ExtractGenotypes.sh
  qsub -hold_jid DataSummary qsub_2_ExtractGenotypes.sh
  cd ..

done

## -----------------------------------------------------------------------------
