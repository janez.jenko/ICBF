#!/usr/bin/env Rscript

ScriptsDir  <- getwd()
AnalysisDir <- "Analysis_Purebreds_LmChAaSiHe"

## Breeding values
BV <- read.csv(file="../EPD_for_FaSTLMM/1M_44_EPDs.csv", header=TRUE, stringsAsFactors=FALSE, fill=TRUE)

for (breed in c("Aa", "Ch", "He", "Lm", "Si")) {
  print(breed)
  Results <- data.frame()
  Breed <- paste0("Pure",breed,"Ind")

  Candidates <- dir(paste0("/home/jjenko/Hpc/Illumina/Janez/", AnalysisDir, "/HaplotypeSummary/", Breed, "/CoreLength20/CarriersOfPotentiallyLethalHaplotypesFromSlidingWindowsAnalysis/"))

  Candidates <- grep('Chr', Candidates, value=TRUE)
  Candidates <- Candidates[!grepl('.pdf', Candidates)]

  Results        <- data.frame(matrix(nrow=22*length(Candidates),ncol=8))
  names(Results) <- c("Haplotype", "Trait","Mean", "Sd", "Intercept", "Status", "Pvalue","Rsquare")

  J <- 0

  for (i in c(1:length(Candidates))) {
    ## Analysed indivduals
    Genotyped <- read.table(file=paste0("/home/jjenko/Hpc/Illumina/Janez/", AnalysisDir, "/SelectedInd/", substr(Candidates[i],1,9), ".txt"), header=FALSE, stringsAsFactors=FALSE)
  
    ## Carriers
    Carriers <- read.table(file=paste0("/home/jjenko/Hpc/Illumina/Janez/", AnalysisDir, "/HaplotypeSummary/", Breed, "/CoreLength20/CarriersOfPotentiallyLethalHaplotypesFromSlidingWindowsAnalysis/", Candidates[i]), header=FALSE, stringsAsFactors=FALSE)
  
    Normal   <- BV[BV$TECHID %in% Genotyped$V2 & !(BV$TECHID %in% Carriers$V1), ]
    Carriers <- BV[BV$TECHID %in% Carriers$V1, ]
  
    for (j in c("CARC_WT", "CARC_CONF", "CARC_FAT", "CULL_COW", "WEAN_WT", "LIVE_WT", "FEED_IN", "CALF_QU", "MAT_WW", "VERY_HVC", "HVC", "MVC", "LVC", "CALF_PR", "COW_SURV", "CIV", "AFC", "CD", "GL", "MORT", "MCD", "DOC")) {
      J <- J + 1
      N             <- data.frame(Normal[[j]], 0)
      C             <- data.frame(Carriers[[j]], 1)
      names(N)      <- names(C) <- c(j, "Status")
      M             <- rbind(N,C)
      R             <- summary(lm(M[[j]]~M[,"Status"]))
      Results[J,1] <- Candidates[i]
      Results[J,2] <- j
      Results[J,3] <- mean(M[[j]], na.rm=TRUE)
      Results[J,4] <- sd(M[[j]], na.rm=TRUE)
      Results[J,5] <- R$coefficients[1]
      Results[J,6] <- R$coefficients[2]
      Results[J,7] <- R$coefficients[2,4]
      Results[J,8] <- R$r.squared
    }
  }
  #Results$Haplotype <- gsub("Ind_.*Chr","Ind_Chr",Results$Haplotype)
  #Results$Breed <- gsub("Ind_.*","Ind",Results$Haplotype)
  Results <- unique(Results)

  ## Calculate allele substitution effect
  Results$AlleleSubstEff <- abs(Results$Status / Results$Sd)

  for (i in unique(Results$Haplotype)) {
    tmpRes     <- Results[Results$Haplotype==i, ]
    tmpRes     <- tmpRes[!is.na(tmpRes$Rsquare), ]
    tmp        <- data.frame(t(tmpRes$AlleleSubstEff))
    names(tmp) <- tmpRes$Trait
    tmp=rbind(rep(0.4,ncol(tmp)) , rep(0,ncol(tmp)) , tmp)
    pdf(file=paste0("/home/jjenko/Hpc/Illumina/Janez/", AnalysisDir, "/HaplotypeSummary/", Breed, "/CoreLength20/CarriersOfPotentiallyLethalHaplotypesFromSlidingWindowsAnalysis/", i, ".pdf"), height=10, width=10)
    radarchart(tmp, title=i, axistype=1, axislabcol="black", caxislabels=seq(0,0.4,0.1))
    dev.off()
  }
}

