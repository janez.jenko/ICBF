#!/bin/bash

########################################
#                                      #
# Haplotypes summary and discovery     #
# of lethal haplotypes                 #
#                                      #
########################################

## Change these locations appropriately
export A=~/Hpc/Illumina/Janez/Analysis_Purebreds_LmChAaSiHe
export S=~/Hpc/Illumina/Janez/Scripts
export prog=~/Hpc/Illumina/Janez/Programs
export HapExcl="1 2 3 4 5 6 11 0.01Perc 0.05Perc 0.5Perc 5Perc"
export CoreLength="5 10 20 50 100 200 500 1000"

## cd ${pedig} || exit
cd ${A} || exit

## Now work on HaplotypeSummary

if [[ ! -d HaplotypeSummary ]]; then
  mkdir HaplotypeSummary
fi

cd HaplotypeSummary || exit

for i in ${A}/SelectedInd/*; do
  a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`
  if [ ! -d ${a} ]; then
    mkdir ${a}
  fi
  cd ${a} || exit
  cp -rf ${S}/qsub_DrawManhattanPlotsForPotentiallyLethalHapAllCores.sh .
  sed -i -e "s/XXBREEDXX/${a}/g" qsub_DrawManhattanPlotsForPotentiallyLethalHapAllCores.sh
  ln -sf ${S}/DrawManhattanPlotsForPotentiallyLethalHapAllCores.R
  ln -sf ${prog}/cpumemlog/cpumemlog.sh
  ln -sf ${prog}/cpumemlog/cpumemlogplot.R
  qsub -hold_jid ExtractPotentiallyLethalHapStep1HowardVanRaden${a}* qsub_DrawManhattanPlotsForPotentiallyLethalHapAllCores.sh
  for k in ${CoreLength}; do
    if [[ ! -d CoreLength${k} ]]; then
      mkdir CoreLength${k}
    fi
    cd CoreLength${k} || exit
    if [[ ! -d LethalCandidatesVanRadenHoward ]]; then
      mkdir LethalCandidatesVanRadenHoward
    fi
    cd LethalCandidatesVanRadenHoward || exit
    cp -rf ${S}/qsub_DrawManhattanPlotsForPotentiallyLethalHap.sh .
    sed -i -e "s/XXBREEDXX/${a}/g" -e "s/XXCORELENGTHXX/${k}/g" qsub_DrawManhattanPlotsForPotentiallyLethalHap.sh
    ln -sf ${S}/DrawManhattanPlotsForPotentiallyLethalHap.R
    ln -sf ${prog}/cpumemlog/cpumemlog.sh
    ln -sf ${prog}/cpumemlog/cpumemlogplot.R
#    qsub -hold_jid ExtractPotentiallyLethalHapStep1HowardVanRaden${a}core${k} qsub_DrawManhattanPlotsForPotentiallyLethalHap.sh
    cd ../..
  done
  cd ..
done
