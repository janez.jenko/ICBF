#!/bin/bash

## Create working directory for AlphaSeqOpt

export A=~/Hpc/Illumina/Janez/Analysis_Purebreds_LmChAaSiHe
export prog=~/Hpc/Illumina/Janez/Programs
export work=~/Hpc/Illumina/Janez

## Load module (this is required for AlphaSeqOpt)
module load intel/2016
module load igmm/apps/R/3.3.0

cd ${A} || exit

for i in ${A}/SelectedInd/*; do

  a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`

  ## Prepare pedigree for AlphaSeqOpt 2 step and file that will define which animals were already genotyed (This is done because pedigree for AlphaPhase and AlphaImpute have only genotyped Id, Sires and Dams)
  cd Pedigree/${a} || exit

  ## This was used only once to create cost file that needs to be changed only if IndAlreadySequenced.txt is going to be changed
  ln -sf ${work}/Scripts/CostsForAlreadySequencedAnimals.R .
  ./CostsForAlreadySequencedAnimals.R 

  ln -sf ${work}/Scripts/AlphaSeqOptPedig.R .

  cat ${work}/Scripts/qsub_AlphaSeqOpt_Pedig.sh | sed -e "s/XXBREEDXX/${a}/g" > qsub_AlphaSeqOpt_Pedig.sh

  rm -f ASeqOptPedig.err ASeqOptPedig.out

  qsub -hold_jid PedExt${a} qsub_AlphaSeqOpt_Pedig.sh

  cd ../../ || exit

done
