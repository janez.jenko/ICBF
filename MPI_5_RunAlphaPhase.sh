#!/bin/bash

########################################
#                                      #
# GE job script for ECDF Cluster       #
#                                      #
########################################

## Change these file locations appropriately
export A=~/Hpc/Illumina/Janez/Analysis_Purebreds_LmChAaSiHe
export S=~/Hpc/Illumina/Janez/Scripts
export prog=~/Hpc/Illumina/Janez/Programs
export fCHR="3" #From chromosomes
export tCHR="3" #To chromosomes

cd ${A}/Chromosomes || exit

for i in ${A}/SelectedInd/*; do
  a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`
  cd ${a} || exit

  for j in `seq ${fCHR} 1 ${tCHR}` ; do

    while [ ! -d Chromosome${j} ]; do
      sleep 10m
    done

    cd Chromosome${j}/ || exit

    ln -sf ${prog}/AlphaSuite/AlphaImputeMPIv1.9.1-2-g5383178 AlphaImputeRO1
    ln -sf ${prog}/AlphaSuite/AlphaImputev1.9-13-gaad8e8c AlphaImputeRO2
    ln -sf ${prog}/cpumemlog/cpumemlog.sh .
    ln -sf ${prog}/cpumemlog/cpumemlogplot.R .

    cat ${S}/qsub_5_RunAlphaPhase_RO1.sh | sed -e "s/XXBREEDXX/${a}/" \
                                               -e "s/XXCHROMXX/${j}/" > qsub_5_RunAlphaPhase_RO1.sh

    cat ${S}/qsub_5_RunAlphaPhase_RO2.sh | sed -e "s/XXBREEDXX/${a}/" \
                                               -e "s/XXCHROMXX/${j}/" > qsub_5_RunAlphaPhase_RO2.sh

    ln -sf ${A}/Pedigree/${a}/AlphaRecodedPedigree.txt input.ped
    ln -sf ch_coded input.gen

    b=`head -n 1 ch_coded | cut -d " " -f 2- | awk '{print NF}'`

    cat ${prog}/AlphaSuite/AlphaImputeSpecMPIv1.9.1-2-g5383178.txt | sed -e "s/XXNROFSNPXX/${b}/" \
                                                                         -e "s/XXNRPROCAVXX/20/" \
                                                                         -e "s/XXBYPASSXX/Yes/" > AlphaImputeSpecTmp.txt

    cat AlphaImputeSpecTmp.txt | sed -e "s/XXRESOPTXX/1/" > AlphaImputeSpecRO1.txt

    cat ${prog}/AlphaSuite/AlphaImputeSpec1.9-13-gaad8e8c.txt | sed -e "s/XXNROFSNPXX/${b}/" \
                                                                    -e "s/XXNRPROCAVXX/20/" \
                                                                    -e "s/XXBYPASSXX/Yes/" > AlphaImputeSpecTmp.txt

    cat AlphaImputeSpecTmp.txt | sed -e "s/XXRESOPTXX/2/" > AlphaImputeSpecRO2.txt
    rm -f AlphaImputeSpecTmp.txt

    qsub -hold_jid PrepChr${a} qsub_5_RunAlphaPhase_RO1.sh
    qsub -hold_jid RO1${a}ch${j} qsub_5_RunAlphaPhase_RO2.sh

    cd - || exit
  done
  cd ${A}/Chromosomes || exit
done

