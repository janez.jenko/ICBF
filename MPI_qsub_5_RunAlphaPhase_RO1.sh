#!/bin/sh
########################################
#                                      #
# GE job script for ECDF Cluster       #
#                                      #
########################################

#
# Grid Engine options
#$ -cwd
#$ -N RO1XXBREEDXXchXXCHROMXX
#$ -o RO1AImpute.out
#$ -e RO1AImpute.err
#$ -pe scatter 10
#$ -l h_vmem=30G
#$ -l h_rt=48:00:00
#$ -P roslin_hickey_group

# Initialise the environment modules
. /etc/profile.d/modules.sh

# Load
module load igmm/apps/R/3.3.0
module load intel/2017u4

#$ -l h='!node1f01.ecdf.ed.ac.uk'
#$ -l h='!node1f02.ecdf.ed.ac.uk'
#$ -l h='!node2j01.ecdf.ed.ac.uk'
#$ -l h='!node2j02.ecdf.ed.ac.uk'
#$ -l h='!node2j03.ecdf.ed.ac.uk'
#$ -l h='!node2j04.ecdf.ed.ac.uk'

# Standard report


echo "Working directory:"
pwd
date

#echo
#echo "System PATH (default):"
#echo $PATH
#echo "System PATH modified:"
export PATH=.:~/bin:$PATH
#echo $PATH
#echo "Ulimit:"
#ulimit -a
#echo

ln -sf AlphaImputeRO1 AlphaImpute 
ln -sf AlphaImputeSpecRO1.txt AlphaImputeSpec.txt

echo "Starting job:"
mpiexec -n 10 ./AlphaImpute > AlphaImputeRO1.log 2>&1 &

# CPU and RAM tracking
JOB=$!
./cpumemlog.sh $JOB #-t=.1
#./cpumemlogplot.R cpumemlog_${JOB}.txt

# Standard report
echo
pwd
date
