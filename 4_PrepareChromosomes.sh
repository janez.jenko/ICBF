#!/bin/bash

export A=~/Hpc/Illumina/Janez/Analysis_Purebreds_LmChAaSiHe
export S=~/Hpc/Illumina/Janez/Scripts
export pedig=~/Hpc/Illumina/Janez/Pedigree

if [ ! -d ${A}/Chromosomes ];
then
  mkdir ${A}/Chromosomes
fi

cd ${A}/Chromosomes || exit

for i in ${A}/SelectedInd/*; do

  a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`

  if [ ! -d ${a} ]; then
    mkdir ${a}
  fi

  cd ${a} || exit

  ln -sf ${A}/Pedigree/${a}/AlphaRecodedPedigree.txt AlphaRecodedPedigree.txt
  ln -sf ${pedig}/Pedigree.txt Pedigree.txt

  rm -f PrepChr.err PrepChr.out

  cp -rf ${S}/qsub_4_PrepareChromosomes.sh .
  sed -i "s/XXBREEDXX/${a}/g" qsub_4_PrepareChromosomes.sh
  qsub -hold_jid AlpRec${a} qsub_4_PrepareChromosomes.sh

  cd ..

done
