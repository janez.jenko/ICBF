#!/bin/sh

#$ -N ASeqOptPedigXXBREEDXX
#$ -cwd
#$ -o ASeqOptPedig.out
#$ -e ASeqOptPedig.err
#$ -l h_rt=24:00:00
#$ -R y
#$ -pe sharedmem 1
#$ -l h_vmem=8G

# Initialise the environment modules
. /etc/profile.d/modules.sh

# Load
module load intel/2016
module load igmm/apps/R/3.3.0

# Standard report

echo "Working directory:"
pwd
date

export OMP_NUM_THREADS=16

./AlphaSeqOptPedig.R > AlphaSeqOptPedig.log

# Standard report
echo
pwd
date
