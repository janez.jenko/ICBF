#!/bin/bash

########################################
#                                      #
# Haplotypes summary and discovery     #
# of lethal haplotypes                 #
#                                      #
########################################

## Change these locations appropriately
export A=~/Hpc/Illumina/Janez/Analysis_Purebreds_LmChAaSiHe
export S=~/Hpc/Illumina/Janez/Scripts
export prog=~/Hpc/Illumina/Janez/Programs
export CoreLength="20"

## cd ${pedig} || exit
cd ${A} || exit

## Now work on HaplotypeSummary

if [[ ! -d HaplotypeSummary ]]; then
  mkdir HaplotypeSummary
fi

cd HaplotypeSummary || exit

for i in ${A}/SelectedInd/*; do
  a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`
  if [ ! -d ${a} ]; then
    mkdir ${a}
  fi
  cd ${a} || exit

  for k in ${CoreLength}; do
    if [[ ! -d CoreLength${k} ]]; then
      mkdir CoreLength${k}
    fi

    cd CoreLength${k} || exit

    cp -rf ${S}/qsub_SlidingWindowsDrawManhattanPlotsForPotentiallyLethalHapAllWindows.sh .
    sed -i -e "s/XXBREEDXX/${a}/g"  -e "s/XXCORELENGTHXX/${k}/g" qsub_SlidingWindowsDrawManhattanPlotsForPotentiallyLethalHapAllWindows.sh
    ln -sf ${S}/SlidingWindowsDrawManhattanPlotsForPotentiallyLethalHapAllWindows.R
    ln -sf ${prog}/cpumemlog/cpumemlog.sh
    ln -sf ${prog}/cpumemlog/cpumemlogplot.R
    qsub -hold_jid SlidingWindowsExtractPotentiallyLethalHapStep1HowardVanRaden${a}core${k}slidingwindow* qsub_SlidingWindowsDrawManhattanPlotsForPotentiallyLethalHapAllWindows.sh

    cd ..
  done
  cd ..
done
