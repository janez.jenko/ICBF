#!/bin/sh

## Change these file locations appropriately
export S=~/Hpc/Illumina/Janez/Scripts
export prog=~/Hpc/Illumina/Janez/Programs
export map=~/Hpc/Illumina/Janez/Gwas/FinalIdbv3.bim
## This is the working directory for current GWAS analysis
export wd=~/Hpc/Illumina/Janez/GwasResults/BreedCorrectionInMatrix

cd ${wd}

## Create links
ln -sf ../../Gwas/FinalIdbv3.bim .
ln -sf ${S}/PlottingGwasIcbf.R .
ln -sf ${S}/FunctionalAnnotationForGwas.R .

## Draw Manhattan and QQ plots
for i in AFC CALF_PR CALF_QU CARC_CONF CARC_FAT CARC_WT CD CIV COW_SURV CULL_COW DOC FEED_IN GL HVC LIVE_WT LVC MAT_WW MCD MORT MVC VERY_HVC WEAN_WT; do
  cat ${S}/qsub_GwasManhattanQqPlotsAnnotation.sh | sed -e "s/XXTRAITXX/${i}/g" > qsub_GwasManhattanQqPlotsAnnotation${i}.sh
  qsub qsub_GwasManhattanQqPlotsAnnotation${i}.sh
done
