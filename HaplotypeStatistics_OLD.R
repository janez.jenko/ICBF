#! /usr/bin/env Rscript

## Load library
library(plyr)
library(Rarity)

#### Set working directory
Dir  <- getwd()
Base <- basename(getwd())

#### Set breed and chromosomes
C <- c(1:29)

################################################################################
##       Prepare data                                                         ##
##       Calculate haplotype frequency                                        ##
##       Count the number of homozygous individuals                           ##
################################################################################

## Set Haplo table
Haplo <- data.frame(Chromosome=integer(),
                    HaploLib=character(),
                    HaploID=integer(),
                    HaploNum=integer())

## Fill in the halpo table
print("Fill in the haplo table")
for (i in C) {
  print(paste("Chromosome", i, sep=" "))
  setwd(paste("../../Chromosomes/", Base, "/Chromosome", i, "/Phasing/PhasingResults/HaplotypeLibrary", sep=""))
  Files <- dir()
  Files <- Files[grep("*.txt", Files) ]
  for (j in Files) {
    H <- data.frame(i, j, read.table(file=j, header=FALSE, stringsAsFactors=FALSE))
    H <- H[,c(-5)]
    names(H) <- c("Chromosome","HaploLib","HaploID","HaploNum")
    Haplo <- rbind.fill(Haplo,H)
  }
  setwd(Dir)
}

## Calculate haplotype frequency within each chromosome and core
print("Calculate haplotype frequency within each group of chromosome and core")
Haplo$NumOfHaploInCore <- NA
Haplo$Perc             <- NA
for (i in C) {
  print(paste("Chromosome", i, sep=" "))
  H <- unique(Haplo[Haplo$Chromosome==i, "HaploLib"])
  for (j in H) {
    tmp              <- Haplo[Haplo$Chromosome==i & Haplo$HaploLib==j, ]
    NumOfHaploInCore <- sum(tmp$HaploNum)
    Haplo[Haplo$Chromosome==i & Haplo$HaploLib==j, "NumOfHaploInCore"] <- NumOfHaploInCore
    Haplo[Haplo$Chromosome==i & Haplo$HaploLib==j, "Perc"]             <- Haplo[Haplo$Chromosome==i & Haplo$HaploLib==j, "HaploNum"]/NumOfHaploInCore * 100
  }
}

## Get the information about the homozygosity/heterozygosity
print("Get the information about the homozygosity/heterozygosity")
is.even   <- function(x) x %% 2 == 0

for (i in C) {
  print(paste("Chromosome", i, sep=" "))
  J           <- 0
  HapIndCarry <- read.table(paste("../../Chromosomes/", Base, "/Chromosome", i, "/Phasing/PhasingResults/FinalHapIndCarry.txt", sep=""), header=FALSE, stringsAsFactors=FALSE)
  Cores       <- c(2 : ncol(HapIndCarry))
  Cores       <- Cores[is.even(Cores)]
  nAnim       <- nrow(HapIndCarry)
  Genotypes   <- matrix(, nrow=nAnim, ncol=length(Cores))
  for (j in Cores) {
    J <- J + 1
    x <- logical(nAnim) #empty numeric vector
    for (k in 1:nAnim) {
      x[k] <- HapIndCarry[k,j] == HapIndCarry[k,j+1] & HapIndCarry[k,j] != -99
    }
    Genotypes[,J] <- x
    tmp           <- data.frame(table(HapIndCarry[x,j]))
    names(tmp)    <- c("HapId","NumberOfHomozygous")
    write.table(tmp, file=paste("HomozygousHaplotypesChromosome", i, "Core", j/2, ".txt", sep=""), col.names=TRUE, row.names=FALSE, quote=FALSE)
  }
  Genotypes <- Genotypes*1
  write.table(Genotypes, file=paste("GenotypesChromosome", i, ".txt", sep=""), col.names=TRUE, row.names=FALSE, quote=FALSE)
}

################################################################################
##       Identify lethal haplotypes                                           ##
################################################################################

print("Identify lethal haplotypes")
print("VanRaden1 approach")

## Put files that are in the folder into a vector
Files <- dir()

## Check for the number of homozygous individuals for haplotypes
Homozygous <- data.frame(Chromosome=integer(),
                         HaploLib=character(),
                         HaploID=integer(),
                         NumberOfHomozygous=integer())

for (i in C) {
  print(paste("Chromosome", i, sep=" "))
  for (j in Files[grep(paste("HomozygousHaplotypesChromosome", i, "Core.*.txt", sep=""), Files) ] ) {
    H <- data.frame(i, paste("HapLib", gsub("^.*Core","",j), sep=""), read.table(file=j, header=TRUE, stringsAsFactors=FALSE))
    read.table(j, header=TRUE, stringsAsFactors=FALSE)
    names(H)   <- c("Chromosome", "HaploLib", "HaploID", "NumberOfHomozygous")
    Homozygous <- rbind.fill(Homozygous,H)
  }
}

## Add the number of all the individuals genoyped to the Haplo table
Haplo$TotGeno <- as.numeric(system(paste("wc -l ../../Chromosomes/", Base, "/Chromosome1/Phasing/PhasingResults/FinalHapIndCarry.txt | cut -d \" \" -f1", sep=""), intern=TRUE))

## Add the number of homozygous individuals from the Homozygous table to the Haplo table
Haplo <- merge(x = Haplo, y = Homozygous, by = c("Chromosome","HaploLib","HaploID"), all.x = TRUE)
Haplo[is.na(Haplo$NumberOfHomozygous), "NumberOfHomozygous"] <- 0

## Calculate the number of carrier animals (they ar heterozygous for the haplotype analysed)
Haplo$Carriers <- Haplo$HaploNum - 2 * Haplo$NumberOfHomozygous

## Calculate the VanRaden 1 number of expected homozygous and probability of not detecing any individuals homozygous for that haplotype
Haplo$ExpectedNumOfHomoVR1 <- (Haplo$TotGeno/4) * (Haplo$Carriers/Haplo$TotGeno)^2
Haplo$ProbOfNoHomozVR1 <- (1 - ((Haplo$Carriers/Haplo$TotGeno)^2)/4)^Haplo$TotGeno

## Write out the table with haplotypes infomation
#write.table(Haplo, file="HaplotypesFreqVR1.txt", row.names=FALSE, col.names=TRUE, quote=FALSE)


## Calculate the VanRaden2 approach - number of expected homozygous and probability of not detecing any individuals homozygous for that haplotype
print("VanRaden2 approach")

## Read the pedigree and prepare it in the format ID, Sire, Dam, MGS, Sex

print("Read Pedigree")
Pedig                          <- read.table(file="/home/jjenko/Hpc/Illumina/Janez/Pedigree/Pedigree.txt", header=TRUE, stringsAsFactors=FALSE, fill=TRUE)
names(Pedig)                   <- c("ID", "Sire", "Dam", "Sex")
Pedig                          <- merge(Pedig, Pedig[,c("ID","Sire")], by.x="Dam", by.y="ID", all.x=TRUE)
Pedig                          <- Pedig[,c("ID", "Sire.x", "Dam", "Sire.y","Sex")]
names(Pedig)                   <- c("ID", "Sire", "Dam", "MGS", "Sex")
Pedig[is.na(Pedig$MGS), "MGS"] <- 0

print("Number of animals in the pedigree")
print(nrow(Pedig))



## The following function creates the number matings where sire and MGS were carriers separately for each haplotype
VR2 <- function(TmpPedig, Output, MGSDam){
  ## Extract matings with know sire and MGS
  ##TmpPedig <- Pedig[Pedig$Sire!=0 & Pedig$MGS!=0, c("Sire", "MGS")]

  ## Keep only unique combination of sire, MGS mating and count the number of these matings
  ##TmpPedig$Count <- 1
  ##TmpPedig       <- aggregate(Count ~ Sire + MGS, data = TmpPedig, FUN = length)

  ## Check if Sire and MGS are carriers for specific haplotype

  ##CoreHaplo <- data.frame(HaploID=double(),
  ##                        HaploNum=integer(),
  ##                        Chromosome=integer(),
  ##                        Core=integer())

  print(paste("Chromosome", i, sep=" "))
  HapIndCarry       <- read.table(paste("../../Chromosomes/", Base, "/Chromosome", i, "/Phasing/PhasingResults/FinalHapIndCarry.txt", sep=""), header=FALSE, stringsAsFactors=FALSE)
  CarrierMating     <- matrix(, nrow=nrow(TmpPedig), ncol=ncol(HapIndCarry)+1)
  HomozygousProgeny <- matrix(, nrow=nrow(TmpPedig), ncol=(ncol(HapIndCarry-1)/2)+1)
  #print(paste(j, "Size of the pedigree data frame", dim(TmpPedig), sep="")
  for (j in 1:nrow(TmpPedig)) {
    Id <- TmpPedig[j,"ID"]
    #print(paste(j, "Id", Id, sep=" "))
    Sire <- TmpPedig[j,"Sire"]
    #print(paste(j, "Sire", Sire, sep=" "))
    MGS  <- TmpPedig[j,"MGS"]
    #print(paste(j, "MGS", MGS, sep=" "))
    Id <- HapIndCarry[HapIndCarry$V1 == Id, ]
    #print(Sire)
    Sire <- HapIndCarry[HapIndCarry$V1 == Sire, ]
    #print(Sire)
    MGS  <- HapIndCarry[HapIndCarry$V1 == MGS, ]
    #print(MGS)
    CoresHaplo <- c(2 : ncol(HapIndCarry))
    Cores      <- CoresHaplo[is.even(CoresHaplo)]
    x          <- integer(2+length(CoresHaplo)) #empty numeric vector
    x[1]       <- Sire[,1]
    x[2]       <- MGS[,1]
    y          <- integer(1+length(Cores)) #empty numeric vector
    y[1]       <- Id[,1]
    for (k in Cores) {
      ## Remove all homozygous (keep only carrires)      Keep only those that share one or both haplotypes and that haplotype is not -99 (missing)
      if ((Sire[,k]!=Sire[,k+1] & MGS[,k]!=MGS[,k+1] & Sire[,k] %in% MGS[,c(k+1,k)] & Sire[,k] != -99) == TRUE) {
        x[k+1] <- Sire[,k]
      }
      if ((Sire[,k]!=Sire[,k+1] & MGS[,k]!=MGS[,k+1] & Sire[,k+1] %in% MGS[,c(k+1,k)] & Sire[,k+1] != -99) == TRUE) {
        x[k+2] <- Sire[,k+1]
      }
      ## Calculate number of homozygous progeny for carrier matings
      if (Id[,k]==Id[,k+1] & (Id[,k] %in% x[c(k+1,k+2)]) & !(Id[,k] %in% c(0,-99))) {
        y[(k/2)+1] <- Id[,k]
      }
    }
    CarrierMating[j,]     <- x
    HomozygousProgeny[j,] <- y
  }

  CoreHaploChr        <- data.frame(HaploID=double(),
                                    NumOfCarrierMating=integer(),
                                    Chromosome=integer(),
                                    Core=integer())

  CoreHaploChrHomProg <- data.frame(HaploID=double(),
                                    NumOfHomozygousProgenyMating=integer(),
                                    Chromosome=integer(),
                                    Core=integer())

  for (l in Cores) {
    ## For carries matings
    tmp                 <- data.frame(table(c(CarrierMating[,l+1], CarrierMating[,l+2])))
    tmp$Chromosome      <- i
    tmp$Core            <- l/2
    names(tmp)          <- c("HaploID", "NumOfCarrierMating", "Chromosome", "Core")
    tmp$HaploID         <- as.numeric(as.character(tmp$HaploID))
    CoreHaploChr        <- rbind.fill(CoreHaploChr, tmp)
    ## For homozygous progeny from carrier matings
    tmp                 <- data.frame(table(c(HomozygousProgeny[,(l/2)+1])))
    tmp$Chromosome      <- i
    tmp$Core            <- l/2
    names(tmp)          <- c("HaploID", "NumOfHomozygousProgenyMating", "Chromosome", "Core")
    tmp$HaploID         <- as.numeric(as.character(tmp$HaploID))
    CoreHaploChrHomProg <- rbind.fill(CoreHaploChrHomProg, tmp)
  }

  ## Remove the 0 HaplotID
  CoreHaploChr        <- CoreHaploChr[CoreHaploChr$HaploID!=0,]
  CoreHaploChrHomProg <- CoreHaploChrHomProg[CoreHaploChrHomProg$HaploID!=0,]

  ## Merge CoreHaploChr and CoreHaploChrHomProg so that you keep all the lines from CoreHaploChr (Carrier matings)
  CoreHaploChr        <- merge(CoreHaploChr, CoreHaploChrHomProg, by.x=c("HaploID","Chromosome","Core"), by.y=c("HaploID","Chromosome","Core"), all.x=TRUE)
  CoreHaploChr[is.na(CoreHaploChr$NumOfHomozygousProgenyMating), "NumOfHomozygousProgenyMating"] <- 0

  if (MGSDam == "MGS") {
    CoreHaploChr$ExpectedNumOfHomoVR2 <- round(CoreHaploChr$NumOfCarrierMating/8, 2)
    CoreHaploChr$ProbOfNoHomozVR2     <- 0.875^CoreHaploChr$NumOfCarrierMating
  } else if (MGSDam == "Dam") {
    CoreHaploChr$ExpectedNumOfHomoVR2 <- CoreHaploChr$NumOfCarrierMating/4
    CoreHaploChr$ProbOfNoHomozVR2     <- 0.75^CoreHaploChr$NumOfCarrierMating
  }

  CoreHaploChr$p                         <- CoreHaploChr$ExpectedNumOfHomoVR2 / CoreHaploChr$NumOfCarrierMating
  CoreHaploChr$ExpectedNumOfHomoVR2CiLow <- (CoreHaploChr$p - 1.96 * ((CoreHaploChr$p * (1 - CoreHaploChr$p)) / CoreHaploChr$NumOfCarrierMating)^0.5) * CoreHaploChr$NumOfCarrierMating
  CoreHaploChr$ExpectedNumOfHomoVR2CiUp  <- (CoreHaploChr$p + 1.96 * ((CoreHaploChr$p * (1 - CoreHaploChr$p)) / CoreHaploChr$NumOfCarrierMating)^0.5) * CoreHaploChr$NumOfCarrierMating
  CoreHaploChr                           <- CoreHaploChr[, !(colnames(CoreHaploChr) %in% c("p"))]

  write.table(CoreHaploChr, file=paste("MatingPatternVR2", Output, "Chr", i, ".txt", sep=""), col.names=TRUE, row.names=FALSE, quote=FALSE)
#    CoreHaplo <- rbind.fill(CoreHaplo, CoreHaploChr[CoreHaploChr$HaploID!=0,])
}

#  CoreHaplo <- CoreHaplo[CoreHaplo$HaploID!=0,]

#  CoreHaplo$ExpectedNumOfHomoVR1 <- round(CoreHaplo$HaploNum/4, 2)
#  CoreHaplo$ProbOfNoHomozVR2     <- 0.75^CoreHaplo$HaploNum
#  return(CoreHaplo)
## Function ends here


## Here we are using 2 approaches for the calculation of carrier matings.

## Store the informartion about the genotyped individuals in the first chromosome.
## For every chromosome later on genotyped individuals are checked and if it is
## different Pedig1 and Pedig2 are updated.
GenotypedPrev        <- data.frame(system(paste("cut -d \" \" -f1 ../../Chromosomes/", Base, "/Chromosome1/Phasing/PhasingResults/FinalHapIndCarry.txt", sep=""), intern=TRUE), stringsAsFactors=FALSE)
names(GenotypedPrev) <- c("ID")
GenotypedPrev$ID     <- as.integer(GenotypedPrev$ID)

for (i in C) {
  Genotyped        <- data.frame(system(paste("cut -d \" \" -f1 ../../Chromosomes/", Base, "/Chromosome", i, "/Phasing/PhasingResults/FinalHapIndCarry.txt", sep=""), intern=TRUE), stringsAsFactors=FALSE)
  names(Genotyped) <- c("ID")
  Genotyped$ID     <- as.integer(Genotyped$ID)

  if (sum(Genotyped$ID %in% GenotypedPrev$ID) != nrow(Genotyped) | nrow(Genotyped) != nrow(GenotypedPrev) | i == 1) {
    ## In the first one progeny, sire and maternal grand sire need to be genotyped
    ## (this is how VanRaden did it)
    Pedig1 <- Pedig[Pedig$ID %in% Genotyped$ID & Pedig$Sire %in% Genotyped$ID & Pedig$MGS %in% Genotyped$ID, ]
    Pedig1 <- Pedig1[Pedig1$Sire!=0 & Pedig1$MGS!=0, c("ID", "Sire", "MGS")]
    print("Number of genotyped trios in pedigree: Individual, Sire, MGS")
    print(nrow(Pedig1))

#    ## In the second one only sire and maternal grand sire need to be genotyped
#    ## (this is the approach where you do not care whether progeny is genotyped or not)
#    Pedig2 <- Pedig[Pedig$Sire %in% Genotyped$ID & Pedig$MGS %in% Genotyped$ID, ]
#    Pedig2 <- Pedig2[Pedig2$Sire!=0 & Pedig2$MGS!=0, c("ID", "Sire", "MGS")]
#    print("Number of genotyped matings Sire and MGS in pedigree: Sire, MGS")
#    print(nrow(Pedig2))

    ## In the third one progeny, sire and dam need to be genotyped
    Pedig3        <- Pedig[Pedig$ID %in% Genotyped$ID & Pedig$Sire %in% Genotyped$ID & Pedig$Dam %in% Genotyped$ID, ]
    Pedig3        <- Pedig3[Pedig3$Sire!=0 & Pedig3$Dam!=0, c("ID", "Sire", "Dam")]
    ## Rename Dam into MGS this is done becase in function MGS is used instead of dam
    names(Pedig3) <- c("ID", "Sire","MGS")
    print("Number of genotyped trios in pedigree: Individual, Sire, Dam")
    print(nrow(Pedig3))
  }

  if(nrow(Pedig1)==0) {
    print("There is no trio of Progeny, Sire and MGS that were genotyped")
  } else {
    print("VR2 IdSireMgs")
    VR2(Pedig1,"IdSireMgs","MGS")
  }

#  if(nrow(Pedig2)==0) {
#    print("There is no pair of Sire and MGS that were genotyped")
#  } else {
#    print("VR2 SireMgs")
#    VR2(Pedig2,"SireMgs","MGS")
#  }

  if(nrow(Pedig3)==0) {
    print("There is no pair of Sire and Dam that were genotyped")
  } else {
    print("VR3 IdSireDam")
    VR2(Pedig3,"IdSireDam","Dam")
  }

  GenotypedPrev <- Genotyped

}

## Remove Pedig data frame as it takes a lot of memory
rm(Pedig)



## Create the final output
MatingPatternVR2 <- data.frame(Chromosome=integer(),
                               HaploID=double(),
                               Core=integer(),
                               NumOfCarrierMatingIdSireMgsChr=double(),
                               NumOfHomozygousProgenyMatingIdSireMgsChr=double(),
                               ExpectedNumOfHomoVR2IdSireMgsChr=double(),
                               ExpectedNumOfHomoVR2CiLowIdSireMgsChr=double(),
                               ExpectedNumOfHomoVR2CiUpIdSireMgsChr=double(),
                               ProbOfNoHomozVR2IdSireMgsChr=double(),
#                               NumOfCarrierMatingSireMgsChr=double(),
#                               NumOfHomozygousProgenyMatingSireMgsChr=double(),
#                               ExpectedNumOfHomoVR2SireMgsChr=double(),
#                               ExpectedNumOfHomoVR2CiLowSireMgsChr=double(),
#                               ExpectedNumOfHomoVR2CiUpSireMgsChr=double(),
#                               ProbOfNoHomozVR2SireMgsChr=double(),
                               NumOfCarrierMatingIdSireDamChr=double(),
                               NumOfHomozygousProgenyMatingIdSireDamChr=double(),
                               ExpectedNumOfHomoVR2IdSireDamChr=double(),
                               ExpectedNumOfHomoVR2CiLowIdSireDamChr=double(),
                               ExpectedNumOfHomoVR2CiUpIdSireDamChr=double(),
                               ProbOfNoHomozVR2IdSireDamChr=double())

for (i in C) {
  MatingIdSireMgsChr <- read.table(file=paste("MatingPatternVR2IdSireMgsChr", i,".txt", sep=""), header=TRUE, stringsAsFactors=TRUE)
#  MatingSireMgsChr   <- read.table(file=paste("MatingPatternVR2SireMgsChr", i,".txt", sep=""), header=TRUE, stringsAsFactors=TRUE)
  MatingIdSireDamChr <- read.table(file=paste("MatingPatternVR2IdSireDamChr", i,".txt", sep=""), header=TRUE, stringsAsFactors=TRUE)
#  Mating             <- merge(MatingIdSireMgsChr, MatingSireMgsChr, by=c("Chromosome","HaploID","Core"), all=TRUE)
  Mating             <- merge(MatingIdSireMgsChr, MatingIdSireDamChr, by=c("Chromosome","HaploID","Core"), all=TRUE)
#  Mating             <- merge(Mating, MatingIdSireDamChr, by=c("Chromosome","HaploID","Core"), all=TRUE)
  names(Mating)      <- c("Chromosome", "HaploID", "Core",
                          "NumOfCarrierMatingIdSireMgsChr", "NumOfHomozygousProgenyMatingIdSireMgsChr", "ExpectedNumOfHomoVR2IdSireMgsChr", "ProbOfNoHomozVR2IdSireMgsChr", "ExpectedNumOfHomoVR2CiLowIdSireMgsChr", "ExpectedNumOfHomoVR2CiUpIdSireMgsChr",
#                          "NumOfCarrierMatingSireMgsChr", "ExpectedNumOfHomoVR2SireMgsChr", "ProbOfNoHomozVR2SireMgsChr",
                          "NumOfCarrierMatingIdSireDamChr", "NumOfHomozygousProgenyMatingIdSireDamChr", "ExpectedNumOfHomoVR2IdSireDamChr", "ProbOfNoHomozVR2IdSireDamChr", "ExpectedNumOfHomoVR2CiLowIdSireDamChr", "ExpectedNumOfHomoVR2CiUpIdSireDamChr")
  MatingPatternVR2   <- rbind.fill(MatingPatternVR2, Mating)
}

MatingPatternVR2$HaploLib <- paste("HapLib", MatingPatternVR2$Core, ".txt", sep="")

FinalResults <- merge(Haplo, MatingPatternVR2, by=c("Chromosome", "HaploLib", "HaploID"), all.x = TRUE)

FinalResults <- FinalResults[,c(-12)]

## Perform Chi-square statistics the difference between expected and observed number of homozygous individuals for MATINGS
FinalResults$ChiSquareStatisticsSireMgs <- ((FinalResults$NumOfHomozygousProgenyMatingIdSireMgsChr - FinalResults$ExpectedNumOfHomoVR2IdSireMgsChr)^2 / FinalResults$ExpectedNumOfHomoVR2IdSireMgsChr) + (((FinalResults$NumOfCarrierMatingIdSireMgsChr - FinalResults$NumOfHomozygousProgenyMatingIdSireMgsChr) - (FinalResults$NumOfCarrierMatingIdSireMgsChr - FinalResults$ExpectedNumOfHomoVR2IdSireMgsChr))^2 / (FinalResults$NumOfCarrierMatingIdSireMgsChr - FinalResults$ExpectedNumOfHomoVR2IdSireMgsChr))
FinalResults$ChiSquareStatisticsSireDam <- ((FinalResults$NumOfHomozygousProgenyMatingIdSireDamChr - FinalResults$ExpectedNumOfHomoVR2IdSireDamChr)^2 / FinalResults$ExpectedNumOfHomoVR2IdSireDamChr) + (((FinalResults$NumOfCarrierMatingIdSireDamChr - FinalResults$NumOfHomozygousProgenyMatingIdSireDamChr) - (FinalResults$NumOfCarrierMatingIdSireDamChr - FinalResults$ExpectedNumOfHomoVR2IdSireDamChr))^2 / (FinalResults$NumOfCarrierMatingIdSireDamChr - FinalResults$ExpectedNumOfHomoVR2IdSireDamChr))

FinalResults$ChiSquareStatisticsSireMgsPvalue <- 1-pchisq(FinalResults$ChiSquareStatisticsSireMgs,1)
FinalResults$ChiSquareStatisticsSireDamPvalue <- 1-pchisq(FinalResults$ChiSquareStatisticsSireDam,1)

write.table(FinalResults, file="FinalResults.txt", col.names=TRUE, row.names=FALSE, quote=FALSE)

################################################################################
##       Some plotting                                                        ##
################################################################################

## Plot haplotypes according to their number of copies
print("Plot haplotypes according to their number of copies")
L <- c(1,2,6,11,21,51,101)
U <- c(1,5,10,20,50,100,9999999999)
S <- c("1","2-5","6-10","11-20","21-50","51-100",">100")
Haplo$ClassNum <- NA
Haplo$SeqNum   <- NA

SeqNum <- 0
for (i in 1:length(L)) {
  SeqNum <- SeqNum + 1
  Haplo[Haplo$HaploNum >= L[i] & Haplo$HaploNum <= U[i],]$ClassNum <- S[i]
  Haplo[Haplo$HaploNum >= L[i] & Haplo$HaploNum <= U[i],]$SeqNum   <- SeqNum
}

Freq     <- data.frame(table(Haplo$ClassNum))
Freq$Seq <- c(7,1,4,2,5,6,3)
Freq     <- Freq[order(Freq$Seq),]
pdf(file="HaplotypesDistribution.pdf", width=18/2.54,height=13/2.54, pointsize=12, encoding="CP1250")
BP <- barplot(Freq$Freq, col = rainbow(20),ylim=c(0,max(Freq$Freq)*1.1),main=Base,names.arg=c("1", "2-5", "6-10", "11-20", "21-50", "51-100", ">100"),
      xlab="Haplotypes number class", ylab="Number of haplotypes")
text(x = BP, y = Freq$Freq, label = Freq$Freq, pos = 3, cex = 0.8)
text(x = 6, y = max(Freq$Freq), label = paste("Number of distinct haplotypes: ", sum(Freq$Freq), sep=""))
text(x = 8.3, y = max(Freq$Freq)*0.86, pos=2, "Haplotype percentage")
I <- 0.8
text(x = 5.8, y = max(Freq$Freq)*I, pos=2, "Class")
text(x = 6.9, y = max(Freq$Freq)*I, pos=2, "Min")
text(x = 7.9, y = max(Freq$Freq)*I, pos=2, "Max")
for (i in 1:length(L)) {
  I <- I - 0.08
  text(x = 5.8, y = max(Freq$Freq)*I, pos=2, S[i])
  text(x = 6.9, y = max(Freq$Freq)*I, pos=2, round(min(Haplo[Haplo$HaploNum>=L[i] & Haplo$HaploNum<=U[i], "Perc"]),3))
  text(x = 7.9, y = max(Freq$Freq)*I, pos=2, round(max(Haplo[Haplo$HaploNum>=L[i] & Haplo$HaploNum<=U[i], "Perc"]),3))
}
dev.off()


## Plot haplotypes according to their percentage
print("Plot haplotypes according to their frequency")
L <- c(0,0.01,0.05,0.50,5)
U <- c(0.01,0.05,0.50,5,100)
S <- c("0<0.01",">=0.01<0.05",">=0.05<0.50",">=0.50<5",">=5")
Haplo$ClassPerc <- NA
Haplo$SeqPerc   <- NA

SeqPerc <- 0
for (i in 1:length(L)) {
  SeqPerc <- SeqPerc + 1
  Haplo[Haplo$Perc >= L[i] & Haplo$Perc < U[i],]$ClassPerc <- S[i]
  Haplo[Haplo$Perc >= L[i] & Haplo$Perc < U[i],]$SeqPerc   <- SeqPerc
}

Freq     <- data.frame(table(Haplo$ClassPerc))
Freq     <- merge(unique(Haplo[,c("ClassPerc","SeqPerc")]), Freq, by.x="ClassPerc", by.y="Var1")
Freq     <- Freq[order(Freq$SeqPerc),]
pdf(file="HaplotypesDistributionPerc.pdf", width=18/2.54,height=13/2.54, pointsize=12, encoding="CP1250")
BP <- barplot(Freq$Freq, col = rainbow(20),ylim=c(0,max(Freq$Freq)*1.1),main=Base,names.arg=c("0<0.01",">=0.01<0.05",">=0.05<0.50",">=0.50<5",">=5"),
      xlab="Haplotypes percentage class", ylab="Number of haplotypes")
text(x = BP, y = Freq$Freq, label = Freq$Freq, pos = 3, cex = 0.8)
text(x = 4, y = max(Freq$Freq), label = paste("Number of distinct haplotypes: ", sum(Freq$Freq), sep=""))
text(x = 5.9, y = max(Freq$Freq)*0.81, pos=2, "Number of haplotypes")
I <- 0.75
text(x = 4, y = max(Freq$Freq)*I, pos=2, "Class")
text(x = 4.9, y = max(Freq$Freq)*I, pos=2, "Min")
text(x = 5.6, y = max(Freq$Freq)*I, pos=2, "Max")
for (i in 1:length(L)) {
  I <- I - 0.08
  text(x = 4, y = max(Freq$Freq)*I, pos=2, paste(S[i], sep=" "))
  text(x = 4.9, y = max(Freq$Freq)*I, pos=2, min(Haplo[Haplo$Perc>=L[i] & Haplo$Perc<=U[i], "HaploNum"]))
  text(x = 5.6, y = max(Freq$Freq)*I, pos=2, max(Haplo[Haplo$Perc>=L[i] & Haplo$Perc<=U[i], "HaploNum"]))
}
dev.off()

## Plot the correlations between different methods for discovery of lethal haplotypes
print("Plot the correlations between different methods for discovery of lethal haplotypes")
tmp          <- FinalResults[,c("ExpectedNumOfHomoVR1", "ExpectedNumOfHomoVR2IdSireMgsChr", "ExpectedNumOfHomoVR2IdSireDamChr" )]
names(tmp)   <- c("VR1", "VR2 IdSireMGS", "VR2 SireDam")
pdf(file="CorrelationPlot.pdf", width=18/2.54,height=13/2.54, pointsize=12, encoding="CP1250")
corPlot(tmp, method = "pearson", pch = 16, cex = .5, digits = 3)
dev.off()

## Extract Haplotypes according to their frequency  and prepare FinalResults.txt for AlphaSeqOpt
print("Extract Haplotypes according to their frequency and prepare FinalResults.txt for AlphaSeqOpt")
FinalResults <- read.table(file="FinalResults.txt", header=TRUE, stringsAsFactors=TRUE)

for (i in C) {
  print(paste("Chromosome", i, sep=" "))
  HapIndCarry <- read.table(paste("../../Chromosomes/", Base, "/Chromosome", i, "/Phasing/PhasingResults/FinalHapIndCarry.txt", sep=""), header=FALSE, stringsAsFactors=FALSE)
  Cores       <- c(2 : ncol(HapIndCarry))
  Cores       <- Cores[is.even(Cores)]
  for (j in c(0.01, 0.05, 0.5, 5)) {
    for (k in Cores) {
      tmp <- FinalResults[FinalResults$Chromosome == i & FinalResults$Perc < j & FinalResults$HaploLib == paste("HapLib", k/2,".txt", sep=""), "HaploID"]
      HapIndCarry[HapIndCarry[,k] %in% unique(tmp), k]     <- -99
      HapIndCarry[HapIndCarry[,k+1] %in% unique(tmp), k+1] <- -99
    }
    MainDir <- paste("../../AlphaSeqOpt/", Base, "/", j, "Perc", sep="")
    CoreDir <- paste("../../AlphaSeqOpt/", Base, "/", j, "Perc/Cores", sep="")
    if (!file.exists(MainDir)) {
      dir.create(MainDir)
    }
    if (!file.exists(CoreDir)) {
        dir.create(CoreDir)
    }
    write.table(HapIndCarry, file=paste(CoreDir, "/Chromosome", i, ".txt", sep=""), col.names=FALSE, row.names=FALSE, quote=FALSE, sep="\t")
  }
}

################################################################################
##       Cleaning                                                             ##
################################################################################

system("rm HomozygousHaplotypesChromosome* MatingPatternVR2SireMgsChr* MatingPatternVR2IdSireMgsChr* MatingPatternVR2IdSireDamChr* GenotypesChromosome*")

################################################################################
##       Script ends here                                                     ##
################################################################################

print("Script has successfully finished")
