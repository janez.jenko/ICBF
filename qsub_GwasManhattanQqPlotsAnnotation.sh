#!/bin/sh

# Grid Engine options
#$ -cwd
#$ -N GwasManhattanQqPlotsAnnotationXXTRAITXX
#$ -o GwasManhattanQqPlotsAnnotationXXTRAITXX.out
#$ -e GwasManhattanQqPlotsAnnotationXXTRAITXX.err
#$ -pe sharedmem 1
#$ -l h_vmem=2G
#$ -l h_rt=2:00:00
#$ -P roslin_hickey_group

# Initialise the environment modules
. /etc/profile.d/modules.sh

# Load modules
module load igmm/apps/R/3.3.0

# Standard report
echo "Working directory:"
pwd
date
echo "Starting job:"

./PlottingGwasIcbf.R plinkFastlmmXXTRAITXX.out.txt
./FunctionalAnnotationForGwas.R XXTRAITXX

