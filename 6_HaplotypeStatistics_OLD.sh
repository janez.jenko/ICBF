#!/bin/bash

########################################
#                                      #
# Haplotypes summary and discovery     #
# of lethal haplotypes                 #
#                                      #
########################################

## Change these locations appropriately
export A=~/Hpc/Illumina/Janez/Analysis_Purebreds_LmChAaSiHe
export S=~/Hpc/Illumina/Janez/Scripts
export prog=~/Hpc/Illumina/Janez/Programs
export HapExcl="1 2 3 4 5 6 11 0.01Perc 0.05Perc 0.5Perc 5Perc"

## cd ${pedig} || exit
cd ${A} || exit

## Create Core folders for running AlphaSeqOpt in the next step.
## They need to be created here as Chromosome files for scenarios with restricted percentages are created here

if [[ ! -d AlphaSeqOpt ]]; then
  mkdir AlphaSeqOpt || exit
fi

cd AlphaSeqOpt || exit

for i in ${A}/SelectedInd/*; do

  a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`

  if [ ! -d ${a} ]; then
    mkdir ${a}
  fi

  cd ${a} || exit

  for j in ${HapExcl}; do
    seq=$(($seq+1))

    if [[ ! -d ${j} ]]; then
      mkdir ${j}
    fi

    cd ${j} || exit

    if [[ ! -d Cores ]]; then
      mkdir Cores
    fi

    cd ..

  done

  cd ..

done

cd ..

## Now work on HaplotypeSummary

if [[ ! -d HaplotypeSummary ]]; then
  mkdir HaplotypeSummary
fi

cd HaplotypeSummary || exit

for i in ${A}/SelectedInd/*; do
  a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`
  if [ ! -d ${a} ]; then
    mkdir ${a}
  fi
  cd ${a} || exit
  cp -rf ${S}/qsub_HaplotypeStatistics.sh .
  sed -i "s/XXBREEDXX/${a}/g" qsub_HaplotypeStatistics.sh
  ln -sf ${S}/HaplotypeStatistics.R
  ln -sf ${prog}/cpumemlog/cpumemlog.sh
  qsub -hold_jid ASeqOpt${a} qsub_HaplotypeStatistics.sh
  cd ..
done
