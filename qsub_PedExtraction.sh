#!/bin/sh

#$ -N PedExtXXBREEDXX
#$ -cwd
#$ -o PedExt.out
#$ -e PedExt.err
#$ -l h_rt=24:00:00
#$ -R y
#$ -pe sharedmem 1
#$ -l h_vmem=4G

echo "Working directory:"
pwd
date

export OMP_NUM_THREADS=8

./PedExtraction > PedExtraction.log

## Since AlphaRecode breaks if the individual in the first line has known parent file needs to be changed with putting animal with unknown parents in the first line
a=`awk '($2==0) && ($3==0)' PedigreeExtracted.txt | head -1`
sed -i "/${a}/d" PedigreeExtracted.txt
sed -i "1i ${a}" PedigreeExtracted.txt

# Standard report
echo
pwd
date
