#!/bin/bash

########################################
#                                      #
# GE job script for ECDF Cluster       #
#                                      #
########################################

## Change these file locations appropriately
export A=~/Hpc/Illumina/Janez/Analysis_Purebreds_LmChAaSiHe
export S=~/Hpc/Illumina/Janez/Scripts
export prog=~/Hpc/Illumina/Janez/Programs
export fCHR="1" #From chromosomes
export tCHR="29" #To chromosomes


## Module option was done only for the purpose of Running AlphaPhase for ICBF data
export MODULE=2


if [ "$MODULE" == "1" ]; then
	echo "304 - Run alphaphase"
	echo
	echo

	cd ${A}/Chromosomes || exit
	for i in ${A}/SelectedInd/*; do
	  a=`echo $i | sed -e 's/.*\///' -e 's/.txt//'`
    cd $a || exit
	  for j in `seq ${fCHR} 1 ${tCHR}` ; do
			while [ ! -d Chromosome${j} ]; do
		    sleep 10m
	    done
	  	cd Chromosome${j} || exit
	    if [ ! -d "Phasing" ]; then
        mkdir Phasing
    	fi
      cd Phasing || exit
  	  ln -sf ${prog}/AlphaSuite/alphaphase .
  	  ln -sf ../ch_coded input.gen
      ln -sf ${A}/Pedigree/${a}/AlphaRecodedPedigree.txt input.ped
			ln -sf ${prog}/cpumemlog/cpumemlog.sh .
			ln -sf ${prog}/cpumemlog/cpumemlogplot.R .

  	  b=`head -n 1 ../ch_coded | cut -d " " -f 2- | awk '{print NF}'`

  	  cat ${prog}/AlphaSuite/AlphaPhaseSpec.txt | sed -e "s/XXNROFSNPXX/${b}/" > AlphaPhaseSpec.txt
      cat ${S}/qsub306.sh | sed -e "s/XXBREEDXX/${a}/" \
    	                          -e "s/XXCHROMXX/${j}/" > qsub306.sh

      rm -f APhase.err APhase.out

	    qsub -hold_jid PrepChr${a} qsub308.sh
  	  sleep 1s
  	  cd ../../ || exit
  	done
    cd ../ || exit
  done
fi


if [ "$MODULE" != "1" ]; then
	export MAXJOBSPAR="800" # Max nr of jobs running in parallel

	if [ -z $1 ]; then
   echo "Choose where to start:
      1. Restart option 1
      2. GeneProb
      3. Restart option 2
      4. Phasing
      5. Restart option 3
      6. GeneProb 2
      7. Restart option 4
      8. Run AlphaPhase for AlphaSeqOpt
      "
	  read PROG
  else
    export PROG=$1
    echo $PROG
  fi

	if [ "$PROG" == "1" ]; then

  	echo "301 - Run AlphaImpute with restart option =1"
    echo
	  echo

  	cd ${A}/Chromosomes || exit
	  for i in ${A}/SelectedInd/*; do
	    a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`
	  	cd ${a} || exit

  	  for j in `seq ${fCHR} 1 ${tCHR}` ; do

				while [ ! -d Chromosome${j} ]; do
			    sleep 10m
		    done

  	    cd Chromosome${j}/ || exit

        ln -sf ${A}/Pedigree/${a}/AlphaRecodedPedigree.txt input.ped
	      ln -sf ch_coded input.gen
				ln -sf ${prog}/cpumemlog/cpumemlog.sh .
				ln -sf ${prog}/cpumemlog/cpumemlogplot.R .

  	    ln -sf ${prog}/AlphaSuite/AlphaImputev1.7.0 AlphaImpute
    	  cat ${S}/qsub301.sh | sed -e "s/XXBREEDXX/${a}/" \
                                             -e "s/XXCHROMXX/${j}/" > qsub301.sh

	      b=`head -n 1 ch_coded | cut -d " " -f 2- | awk '{print NF}'`

  	    cat ${prog}/AlphaSuite/AlphaImputeSpec.txt | sed -e "s/XXNROFSNPXX/${b}/" \
	                                                       -e "s/XXNRPROCAVXX/20/" \
	                                                       -e "s/XXRESOPTXX/1/" \
	                                                       -e "s/XXBYPASSXX/No/" > AlphaImputeSpec1.txt

        ln -sf AlphaImputeSpec1.txt AlphaImputeSpec.txt

  	    while [ $(qstat | wc -l) -gt ${MAXJOBSPAR} ]  ; do
	        echo "Queue limit reached (${MAXJOBSPAR} jobs). Waiting ..."
	        sleep 1m
  	    done

  	    qsub -hold_jid PrepChr${a} qsub301.sh

	      cd - || exit
  	  done

    	cd ${A}/Chromosomes || exit

    done

  elif [ "$PROG" == "2" ]; then

	  echo "302 - Run GeneProb"
	  echo
	  echo

  	cd ${A}/Chromosomes || exit
	  for i in ${A}/SelectedInd/*; do
      a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`
	  	cd ${a} || exit

    	for j in `seq ${fCHR} 1 ${tCHR}` ; do

				while [ ! -d Chromosome${j} ]; do
			    sleep 10m
		    done

	      cd Chromosome${j}/ || exit

        for k in GeneProb/*; do
	        if [ -d "${k}" ]; then
          	echo ${k}
    	  	  cd ${k} || exit
    	    	ln -sf ${prog}/AlphaSuite/GeneProbForAlphaImpute .
						ln -sf ${prog}/cpumemlog/cpumemlog.sh .
						ln -sf ${prog}/cpumemlog/cpumemlogplot.R .

            while [ $(qstat | wc -l) -gt ${MAXJOBSPAR} ]  ; do
    	      	echo "Queue limit reached (${MAXJOBSPAR} jobs). Waiting ..."
            	sleep 1m
            done

            export g
            g=$(echo ${k} | cut -c 18-)

	          cat ${S}/qsub302.sh | sed -e "s/XXBREEDXX/${a}/" \
                                      -e "s/XXCHROMXX/${j}/" \
                                      -e "s/XXGENEPROBXX/${g}/" > qsub302.sh

	        	qsub -hold_jid P1${a}ch${j} qsub302.sh
            cd - || exit
    	    	sleep 1s
	  	    fi
	    	done
	  	  cd ${A}/Chromosomes/${a} || exit
  	  done
	    cd ${A}/Chromosomes || exit
	  done

	elif [ "$PROG" == "3" ]; then

  	echo "303 - Run AlphaImpute with restart option =2"
	  echo
	  echo

	  cd ${A}/Chromosomes || exit
	  for i in ${A}/SelectedInd/*; do
	    a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`
      cd ${a} || exit

    	for j in `seq ${fCHR} 1 ${tCHR}` ; do

				while [ ! -d Chromosome${j} ]; do
			    sleep 10m
		    done

	      cd Chromosome${j}/ || exit

  	    b=`head -n 1 ch_coded | cut -d " " -f 2- | awk '{print NF}'`

  	  	ln -sf ${prog}/AlphaSuite/AlphaImputev1.7.0 AlphaImpute
	  	  cat ${S}/qsub303.sh | sed -e "s/XXBREEDXX/${a}/" \
                                  -e "s/XXCHROMXX/${j}/" > qsub303.sh

	    	cat ${prog}/AlphaSuite/AlphaImputeSpec.txt | sed -e "s/XXNROFSNPXX/${b}/" \
                                                      	 -e "s/XXNRPROCAVXX/20/" \
                                                         -e "s/XXRESOPTXX/2/" \
                                                         -e "s/XXBYPASSXX/No/" > AlphaImputeSpec2.txt

        ln -sf AlphaImputeSpec2.txt AlphaImputeSpec.txt
				ln -sf ${prog}/cpumemlog/cpumemlog.sh .
				ln -sf ${prog}/cpumemlog/cpumemlogplot.R .

        while [ $(qstat | wc -l) -gt ${MAXJOBSPAR} ]  ; do
	        echo "Queue limit reached (${MAXJOBSPAR} jobs). Waiting ..."
	        sleep 1m
        done

        qsub -hold_jid P2${a}ch${j}gp1,P2${a}ch${j}gp2,P2${a}ch${j}gp3,P2${a}ch${j}gp4,P2${a}ch${j}gp5,P2${a}ch${j}gp6,P2${a}ch${j}gp7,P2${a}ch${j}gp8,P2${a}ch${j}gp9,P2${a}ch${j}gp10,P2${a}ch${j}gp11,P2${a}ch${j}gp12,P2${a}ch${j}gp13,P2${a}ch${j}gp14,P2${a}ch${j}gp15,P2${a}ch${j}gp16,P2${a}ch${j}gp17,P2${a}ch${j}gp18,P2${a}ch${j}gp19,P2${a}ch${j}gp20 qsub303.sh
        sleep 1s
        cd - || exit
    	done
    	cd ${A}/Chromosomes || exit
    done

	elif [ "$PROG" == "4" ]; then

  	echo "304 - Run alphaphase"
  	echo
  	echo

  	cd ${A}/Chromosomes || exit
  	for i in ${A}/SelectedInd/*; do
	  	a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`
	  	cd ${a} || exit

    	for j in `seq ${fCHR} 1 ${tCHR}` ; do

				while [ ! -d Chromosome${j} ]; do
			    sleep 10m
		    done

	      cd Chromosome${j}/ || exit

  	    for k in Phasing/*; do
	        if [ -d "${k}" ]; then
  	        cd ${k} || exit
    	      ln -sf ${prog}/AlphaSuite/alphaphase .
						ln -sf ${prog}/cpumemlog/cpumemlog.sh .
						ln -sf ${prog}/cpumemlog/cpumemlogplot.R .

          	while [ $(qstat | wc -l) -gt ${MAXJOBSPAR} ]  ; do
	            echo "Queue limit reached (${MAXJOBSPAR} jobs). Waiting ..."
      	      sleep 1m
            done

            export b
            b="${k//[!0-9]/}"

            cat ${S}/qsub304.sh | sed -e "s/XXBREEDXX/${a}/" \
                                      -e "s/XXCHROMXX/${j}/" \
                                      -e "s/XXPHASEROUNDXX/${b}/" > qsub304.sh

  	        qsub -hold_jid P3${i}ch${j} qsub304.sh
	          cd - || exit
	          sleep 1s

	        fi
	      done
      	cd ${A}/Chromosomes/${a} || exit
      done
      cd ${A}/Chromosomes || exit
  	done

	elif [ "$PROG" == "5" ]; then

	  echo "305 - Run AlphaImpute with restart option =3"
	  echo
  	echo

    cd ${A}/Chromosomes || exit

  	for i in ${A}/SelectedInd/*; do
	  	a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`
	  	cd ${a} || exit

    	for j in `seq ${fCHR} 1 ${tCHR}` ; do

				while [ ! -d Chromosome${j} ]; do
			    sleep 10m
		    done

        cd Chromosome${j}/ || exit

      	ln -sf ${prog}/AlphaSuite/AlphaImputev1.7.0 AlphaImpute
	      cat ${S}/qsub305.sh | sed -e "s/XXBREEDXX/${a}/" \
                                  -e "s/XXCHROMXX/${j}/" > qsub305.sh

	      b=`head -n 1 ch_coded | cut -d " " -f 2- | awk '{print NF}'`

    	  cat ${prog}/AlphaSuite/AlphaImputeSpec.txt | sed -e "s/XXNROFSNPXX/${b}/" \
	                          	                        	 -e "s/XXNRPROCAVXX/20/" \
	                                                       -e "s/XXRESOPTXX/3/" \
	                                                       -e "s/XXBYPASSXX/No/" > AlphaImputeSpec3.txt

        ln -sf AlphaImputeSpec3.txt AlphaImputeSpec.txt
				ln -sf ${prog}/cpumemlog/cpumemlog.sh .
				ln -sf ${prog}/cpumemlog/cpumemlogplot.R .

	      qsub -hold_jid P4AP${a}Chr${j}Ph1,P4AP${a}Chr${j}Ph2,P4AP${a}Chr${j}Ph3,P4AP${a}Chr${j}Ph4,P4AP${a}Chr${j}Ph5,P4AP${a}Chr${j}Ph6,P4AP${a}Chr${j}Ph7,P4AP${a}Chr${j}Ph8,P4AP${a}Chr${j}Ph9,P4AP${a}Chr${j}Ph10,P4AP${a}Chr${j}Ph11,P4AP${a}Chr${j}Ph12,P4AP${a}Chr${j}Ph13,P4AP${a}Chr${j}Ph14,P4AP${a}Chr${j}Ph15,P4AP${a}Chr${j}Ph16,P4AP${a}Chr${j}Ph17,P4AP${a}Chr${j}Ph18,P4AP${a}Chr${j}Ph19,P4AP${a}Chr${j}Ph20 qsub305.sh

  	  	cd - || exit
      done
      cd ${A}/Chromosomes || exit
    done

	elif [ "$PROG" == "6" ]; then

    echo "305 - Run GeneProb...AGAIN -.-"
	  echo
  	echo

    cd ${A}/Chromosomes || exit
  	for i in ${A}/SelectedInd/*; do
	  	a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`
	  	cd ${a} || exit

    	for j in `seq ${fCHR} 1 ${tCHR}` ; do

				while [ ! -d Chromosome${j} ]; do
			    sleep 10m
		    done

        cd Chromosome${j}/ || exit

        for k in IterateGeneProb/*; do
	  	    if [ -d "${k}" ]; then
        	  echo ${k}
	        	cd ${k} || exit
            ln -sf ${prog}/AlphaSuite/GeneProbForAlphaImpute .
						ln -sf ${prog}/cpumemlog/cpumemlog.sh .
						ln -sf ${prog}/cpumemlog/cpumemlogplot.R .

            export g
            g=$(echo ${k} | cut -c 18-)

            cat ${S}/qsub302.sh | sed -e "s/XXBREEDXX/${a}/" \
                                      -e "s/XXCHROMXX/${j}/" \
                                      -e "s/XXGENEPROBXX/${g}/" > qsub302.sh

            while [ $(qstat | wc -l) -gt ${MAXJOBSPAR} ]  ; do
	            echo "Queue limit reached (${MAXJOBSPAR} jobs). Waiting ..."
              sleep 1m
    	  	  done

	      	  qsub -hold_jid P5${a}ch${j} qsub302.sh
            cd - || exit
    	  	  sleep 1s
          fi
        done
        cd ${A}/Chromosomes/${a} || exit
	    done
      cd ${A}/Chromosomes || exit
    done

	elif [ "$PROG" == "7" ]; then

	  echo "305 - Run AlphaImpute with restart option =4"
  	echo
	  echo

    cd ${A}/Chromosomes || exit

  	for i in ${A}/SelectedInd/*; do
	  	a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`
	  	cd ${a} || exit

    	for j in `seq ${fCHR} 1 ${tCHR}` ; do

				while [ ! -d Chromosome${j} ]; do
			    sleep 10m
		    done

        cd Chromosome${j}/ || exit

				ln -sf ${prog}/cpumemlog/cpumemlog.sh .
				ln -sf ${prog}/cpumemlog/cpumemlogplot.R .

        ln -sf ${prog}/AlphaSuite/AlphaImputev1.7.0 AlphaImpute
        cat ${S}/qsub301.sh | sed -e "s/XXBREEDXX/${a}/" \
                                             -e "s/XXCHROMXX/${j}/" > qsub305.sh

	      a=`head -n 1 ch${j}_coded | cut -d " " -f 2- | awk '{print NF}'`

    	  cat ${prog}/AlphaImputeSpec.txt | sed -e "s/XXNROFSNPXX/${a}/" \
                                              -e "s/XXNRPROCAVXX/20/" \
	                  	                     	  -e "s/XXRESOPTXX/4/" \
	                                            -e "s/XXBYPASSXX/No/" > AlphaImputeSpec4.txt

        rm -f AlphaSimSpec.txt && ln -sf AlphaImputeSpec4.txt AlphaImputeSpec.txt

        qsub -hold_jid G1${a}ch${j}gp1,G1${a}ch${j}gp2,G1${a}ch${j}gp3,G1${a}ch${j}gp4,G1${a}ch${j}gp5,G1${a}ch${j}gp6,G1${a}ch${j}gp7,G1${a}ch${j}gp8,G1${a}ch${j}gp9,G1${a}ch${j}gp10,G1${a}ch${j}gp11,G1${a}ch${j}gp12,G1${a}ch${j}gp13,G1${a}ch${j}gp14,G1${a}ch${j}gp15,G1${a}ch${j}gp16,G1${a}ch${j}gp17,G1${a}ch${j}gp18,G1${a}ch${j}gp19,G1${a}ch${j}gp20 qsub305.sh
        sleep 1s
  	    qsub qsub305.sh

        cd - || exit
      done
      cd ${A}/Chromosomes || exit
    done

  elif [ "$PROG" == "8" ]; then

	  echo "308 - Run alphaphase"
  	echo
  	echo

  	cd ${A}/Chromosomes || exit
  	for i in ${A}/SelectedInd/*; do
      a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`
      cd ${a} || exit
      for j in `seq ${fCHR} 1 ${tCHR}` ; do

				while [ ! -d Chromosome${j} ]; do
			    sleep 10m
		    done

      	cd Chromosome${j} || exit

      	if [ ! -d "Phasing" ]; then
          mkdir Phasing
      	fi

      	cd Phasing || exit
				ln -sf ${prog}/cpumemlog/cpumemlog.sh .
				ln -sf ${prog}/cpumemlog/cpumemlogplot.R .
      	ln -sf ${prog}/AlphaSuite/alphaphase_AlphaSeqOpt alphaphase
      	ln -sf ../Results/ImputeGenotypesHMM.txt input.gen
##      	ln -sf ../Results/ImputePhaseHMM.txt input.gen
      	ln -sf ${A}/Pedigree/${a}/AlphaRecodedPedigree.txt input.ped

        if [ -e ../ch_coded ]; then
        	b=`head -n 1 ../ch_coded | cut -d " " -f 2- | awk '{print NF}'`
        else
					b=`head -n 1 ../Results/ImputeGenotypesHMM.txt | cut -d " " -f 2- | awk '{print NF}'`
				fi

	      cat ${prog}/AlphaSuite/AlphaPhaseSpec_AlphaSeqOpt.txt | sed -e "s/XXNROFSNPXX/${b}/" > AlphaPhaseSpec.txt
      	cat ${S}/qsub308.sh | sed -e "s/XXBREEDXX/${a}/" \
    	                            -e "s/XXCHROMXX/${j}/" > qsub308.sh

      	rm -f APhase.err APhase.out

	      qsub -hold_jid P5${a}ch${j} qsub308.sh
      	sleep 1s
	      cd ${A}/Chromosomes/${a} || exit
	  	done
	  	cd ${A}/Chromosomes || exit
  	done
  fi
fi
