#!/bin/bash

export A=~/Hpc/Illumina/Janez/Analysis_Purebreds_LmChAaSiHe
export S=~/Hpc/Illumina/Janez/Scripts
export CoreLength="10 50 100 200 500 1000"

cd ${A}/HaplotypeSummary || exit

for i in ${A}/SelectedInd/*; do

  a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`

  cd ${a} || exit

  for j in ${CoreLength}; do

    cd CoreLength${j} || exit

    ln -sf ${S}/PedigreeTracking.R
    cp -rf ${S}/qsub_12_PedigreeTracking.sh .
    sed -i -e "s/XXBREEDXX/${a}/g" -e "s/XXCORELENGTHXX/${j}/g" qsub_12_PedigreeTracking.sh
    qsub -hold_jid HaplotypeStatistics${a} qsub_12_PedigreeTracking.sh

    cd ..

  done

  cd ..

done
