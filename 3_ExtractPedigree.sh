#!/bin/bash

########################################
#                                      #
# Create pedigree on restricted number #
#                                      #
########################################

## Change these locations appropriately
export A=~/Hpc/Illumina/Janez/Analysis_Purebreds_LmChAaSiHe
export S=~/Hpc/Illumina/Janez/Scripts
export prog=~/Hpc/Illumina/Janez/Programs
export pedig=~/Hpc/Illumina/Janez/Pedigree

## cd ${pedig} || exit
cd ${A} || exit

if [[ ! -d Pedigree ]]; then
  mkdir Pedigree
fi

cd Pedigree || exit

for i in ${A}/SelectedInd/*; do
  a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`
  if [ ! -d ${a} ]; then
    mkdir ${a}
  fi
  cd ${a} || exit
  ln -sf ${prog}/cpumemlog/cpumemlog.sh .
  ln -sf ${prog}/cpumemlog/cpumemlogplot.R .
  ln -sf ${prog}/PedigreeExtraction/PedExtraction
  cp -rf ${prog}/PedigreeExtraction/PedExtractionSpec.txt .
  cp -rf ${S}/qsub_PedExtraction.sh .
  sed -i "s/XXBREEDXX/${a}/g" qsub_PedExtraction.sh
  ln -sf ${pedig}/PedReordered.txt
  ln -sf ${pedig}/PedigreeRecoded.txt
  cut -d " " -f2 ${A}/GenotypesQualityCheck/${a}/AllChipsFinal.ped > ListId.txt
  qsub -hold_jid PrepGeno${a} qsub_PedExtraction.sh
  ln -sf ${prog}/AlphaSuite/AlphaRecode
  ln -sf ${prog}/AlphaSuite/AlphaRecodeSpec.txt
  cp -rf ${S}/qsub_AlphaRecode.sh .
  sed -i "s/XXBREEDXX/${a}/g" qsub_AlphaRecode.sh
  qsub -hold_jid PedExt${a} qsub_AlphaRecode.sh
  cd ..
done
