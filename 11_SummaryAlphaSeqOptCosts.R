#!/usr/bin/env Rscript

## Extract information from AlphaSeqOpt Costs Analysis_Min50Perc_LmChAaSiHe

A="~/Hpc/Illumina/Janez/Analysis_Purebreds_LmChAaSiHe"
S="~/Hpc/Illumina/Janez/Scripts"
## HapExcl=c("1","2","3","4","5","6","11","0.01Perc","0.05Perc","0.5Perc","5Perc")
HapExcl=c("0.5Perc")

Files <- dir(paste(A, "/SelectedInd", sep=""))

for (i in Files) {
  Breed <- print(gsub(".txt", "", i))
  for (j in HapExcl) {
    setwd(paste(A, "/AlphaSeqOpt/", Breed, "/", j, sep=""))
    tmp <- read.table("OutputCoverageFinal.txt", header=TRUE, stringsAsFactors=FALSE)
    tmp <- tmp[tmp$Coverage != 0, ]
    write.table(tmp, "IndToSequenceToReach80PercCoverageApproach2_Costs.txt", col.names=TRUE, row.names=FALSE, quote=FALSE)
  }
}
