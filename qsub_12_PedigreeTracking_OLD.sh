#!/bin/sh

#$ -N PedigTrackingXXBREEDXX
#$ -cwd
#$ -o PedigTracking.out
#$ -e PedigTracking.err
#$ -l h_rt=2:00:00
#$ -R y
#$ -pe sharedmem 1
#$ -l h_vmem=8G

# Initialise the environment modules
. /etc/profile.d/modules.sh

# Load
module load igmm/apps/R/3.3.0

# Standard report

echo "Working directory:"
pwd
date

## This command was added since the file from the last job are not available immediately
sleep 1m

./PedigreeTracking.R

# Standard report
echo
pwd
date
