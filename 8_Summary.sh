#!/bin/bash

export A=~/Hpc/Illumina/Janez/Analysis_Purebreds_LmChAaSiHe
export S=~/Hpc/Illumina/Janez/Scripts
export HapExcl="1 2 3 4 5 6 11 0.01Perc 0.05Perc 0.5Perc 5Perc"

cd ${A}/AlphaSeqOpt || exit

for i in ${A}/SelectedInd/*; do

  a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`

  cd ${a} || exit

  for j in ${HapExcl}; do

    cd ${j} || exit

    ln -sf ${S}/SummarySummary.R
    cp -rf ${S}/qsub_8_Summary.R .
    sed -i -e "s/XXBREEDXX/${a}/g" -e "s/XXHAPLOTHRESXX/${j}/" qsub_8_Summary.R
    qsub -hold_jid ASeqOpt${a} qsub_8_Summary.R

    cd ..

  done

  cd ..

done
