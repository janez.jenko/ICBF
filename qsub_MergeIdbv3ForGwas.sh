#!/bin/sh

# Grid Engine options
#$ -cwd
#$ -N MergeIdbv3GenotypesForGwas
#$ -o MergeIdbv3GenotypesForGwas.out
#$ -e MergeIdbv3GenotypesForGwas.err
#$ -pe sharedmem 2
#$ -l h_vmem=64G
#$ -l h_rt=96:00:00
#$ -P roslin_hickey_group

# Path to the folder with scripts
export S=~/Hpc/Illumina/Janez/Scripts

# Initialise the environment modules
. /etc/profile.d/modules.sh

# Load modules
module load roslin/plink/3.36-beta
module load igmm/apps/R/3.3.0

# Standard report
echo "Working directory:"
pwd
date
echo "Starting job:"

cd ..

if [ ! -d Gwas ]; then
  mkdir Gwas
fi

cd Gwas

## Because there is a chromosome number 99 I will change it to 30 as otherwise job will not go through
awk '{gsub("99","30",$1)}1' ../Genotypes/idbv3-1.map | awk '{ print toupper($0) }' > idbv3-1.map
awk '{gsub("99","30",$1)}1' ../Genotypes/idbv3-2.map | awk '{ print toupper($0) }' > idbv3-2.map
awk '{gsub("99","30",$1)}1' ../Genotypes/idbv3-3.map | awk '{ print toupper($0) }' > idbv3-3.map
awk '{gsub("99","30",$1)}1' ../Genotypes/idbv3-4.map | awk '{ print toupper($0) }' > idbv3-4.map
awk '{gsub("99","30",$1)}1' ../Genotypes/idbv3-5.map | awk '{ print toupper($0) }' > idbv3-5.map
awk '{gsub("99","30",$1)}1' ../Genotypes/idbv3-6.map | awk '{ print toupper($0) }' > idbv3-6.map
awk '{gsub("99","30",$1)}1' ../Genotypes/idbv3-7.map | awk '{ print toupper($0) }' > idbv3-7.map
awk '{ print toupper($0) }' ../Genotypes/idbv3.map > idbv3.map
ln -sf ../Genotypes/idbv3.ped .
ln -sf ../Genotypes/idbv3-1.ped .
ln -sf ../Genotypes/idbv3-2.ped .
ln -sf ../Genotypes/idbv3-3.ped .
ln -sf ../Genotypes/idbv3-4.ped .
ln -sf ../Genotypes/idbv3-5.ped .
ln -sf ../Genotypes/idbv3-6.ped .
ln -sf ../Genotypes/idbv3-7.ped .

## Create file with names of the plink files we are going to merge
echo "idbv3-1.ped idbv3-1.map" > Idbv3FilesToMerge.txt
echo "idbv3-2.ped idbv3-2.map" >> Idbv3FilesToMerge.txt
echo "idbv3-3.ped idbv3-3.map" >> Idbv3FilesToMerge.txt
echo "idbv3-4.ped idbv3-4.map" >> Idbv3FilesToMerge.txt
echo "idbv3-5.ped idbv3-5.map" >> Idbv3FilesToMerge.txt
echo "idbv3-6.ped idbv3-6.map" >> Idbv3FilesToMerge.txt
echo "idbv3-7.ped idbv3-7.map" >> Idbv3FilesToMerge.txt

## From three different map files (1. idbv3.map, 2. chips from idbv3-1.map to idbv3-5.map are the same, 3. idbv3-6.map) only markers on idbv3.map will be kept
## From markers on idbv3.map only those the same location and name from the others will be used
rm SelectSnps.R

cat << EOF > SelectSnps.R
#!/usr/bin/env Rscript

## Read in map files of all the three map files
Map3   <- read.table(file="idbv3.map", header=FALSE, stringsAsFactors=FALSE)
Map3_1 <- read.table(file="idbv3-1.map", header=FALSE, stringsAsFactors=FALSE)
Map3_6 <- read.table(file="idbv3-6.map", header=FALSE, stringsAsFactors=FALSE)
Map3_7 <- read.table(file="idbv3-7.map", header=FALSE, stringsAsFactors=FALSE)

# ## This is the test if all the marker names in in upper case
# setdiff(toupper(Map3\$V2), Map3\$V2)
# setdiff(toupper(Map3_1\$V2), Map3_1\$V2)
# setdiff(toupper(Map3_6\$V2), Map3_6\$V2)

## Since names of SNPs in the Map3_6 file are in lower case I need change them into uppercase
Map3_6\$V2 <- toupper(Map3_6\$V2)

# ## Check how many markers is mission on a chip that are on the orther chips
# dim(Map3[!(Map3\$V2 %in% intersect(Map3\$V2,Map3_1\$V2)),])
# # 0
# dim(Map3[!(Map3\$V2 %in% intersect(Map3\$V2,Map3_6\$V2)),])
# # 0

# dim(Map3_1[!(Map3_1\$V2 %in% intersect(Map3_1\$V2,Map3\$V2)),])
# # 826
# dim(Map3_1[!(Map3_1\$V2 %in% intersect(Map3_1\$V2,Map3_6\$V2)),])
# # 678

# dim(Map3_6[!(Map3_6\$V2 %in% intersect(Map3_6\$V2,Map3\$V2)),])
# # 148
# dim(Map3_6[!(Map3_6\$V2 %in% intersect(Map3_6\$V2,Map3_1\$V2)),])
# # 0

## Check if there is any SNP that has diffent locations on different chips
Map3\$V5 <- apply( Map3, 1 , paste , collapse = "_" )
Map3\$V5 <- gsub(" ", "", Map3\$V5)
Map3_1\$V5 <- apply( Map3_1, 1 , paste , collapse = "_" )
Map3_1\$V5 <- gsub(" ", "", Map3_1\$V5)
Map3_6\$V5 <- apply( Map3_6, 1 , paste , collapse = "_" )
Map3_6\$V5 <- gsub(" ", "", Map3_6\$V5)
Map3_7\$V5 <- apply( Map3_7, 1 , paste , collapse = "_" )
Map3_7\$V5 <- gsub(" ", "", Map3_7\$V5)

# Before <- dim(Map3)
# # 52542

## SNPs that have the same location (Chromosome number and physical position) and name
Map3 <- Map3[(Map3\$V5 %in% Map3_1\$V5) & (Map3\$V5 %in% Map3_6\$V5) & (Map3\$V5 %in% Map3_7\$V5), c("V1","V2","V3","V4")]

# After <- dim(Map3)
# # 52034

# RemovedSnps <- Before - After
## 508

## Exclude SNPs that have the same location (Chromosome number and physical position) and different name
Map3\$V6 <- apply( Map3[,c("V1","V4")], 1 , paste , collapse = "_" )
Map3\$V6 <- gsub(" ", "", Map3\$V6)
Doubled <- data.frame(table(Map3\$V6))
Doubled <- Doubled[Doubled\$Freq > 1, ]
Doubled <- Map3[Map3\$V6 %in% Doubled\$Var1, ]

Doubled\$V7 <- NA
Previous <- 9999999
for (i in 1:nrow(Doubled)) {
  Next <- Doubled[i,c("V6")]
  if (Next == Previous) {
    Doubled[i,"V7"] <- Doubled[i - 1,"V7"] + 1
  } else {
    Doubled[i,"V7"] <- 1
  }
  Previous <- Doubled[i,"V6"]
}

Map3 <- Map3[!(Map3\$V2 %in% Doubled[Doubled\$V7 > 1, "V2"]), ]

write.table(Map3\$V2, file="SnpsOnIdbv3.txt", col.names=FALSE, row.names=FALSE, quote=FALSE)
EOF

chmod +700 SelectSnps.R

./SelectSnps.R


rm DoubledSamples.R

cat << EOF > DoubledSamples.R
#!/usr/bin/env Rscript

options(width=300)

## Identify doubled samples
## Load the ID of genotypes
ID3   <- read.table(pipe("cut -d ' ' -f2 idbv3.ped"))
ID3_1 <- read.table(pipe("cut -d ' ' -f2 idbv3-1.ped"))
ID3_2 <- read.table(pipe("cut -d ' ' -f2 idbv3-2.ped"))
ID3_3 <- read.table(pipe("cut -d ' ' -f2 idbv3-3.ped"))
ID3_4 <- read.table(pipe("cut -d ' ' -f2 idbv3-4.ped"))
ID3_5 <- read.table(pipe("cut -d ' ' -f2 idbv3-5.ped"))
ID3_6 <- read.table(pipe("cut -d ' ' -f2 idbv3-6.ped"))
ID3_7 <- read.table(pipe("cut -d ' ' -f2 idbv3-7.ped"))

## Ceck which samples are doubled and extract them
ID <- c(ID3\$V1, ID3_1\$V1, ID3_2\$V1, ID3_3\$V1, ID3_4\$V1, ID3_5\$V1, ID3_6\$V1, ID3_7\$V1)
ID <- data.frame(t(table(ID)))

ID <- ID[ID\$Freq > 1, ]

## Add logical values if genoype is on specific SNP
ID\$idbv3   <- ID\$ID %in% ID3\$V1
ID\$idbv3_1 <- ID\$ID %in% ID3_1\$V1
ID\$idbv3_2 <- ID\$ID %in% ID3_2\$V1
ID\$idbv3_3 <- ID\$ID %in% ID3_3\$V1
ID\$idbv3_4 <- ID\$ID %in% ID3_4\$V1
ID\$idbv3_5 <- ID\$ID %in% ID3_5\$V1
ID\$idbv3_6 <- ID\$ID %in% ID3_6\$V1
ID\$idbv3_7 <- ID\$ID %in% ID3_7\$V1

## Write out results
sink(file = "DoubledSamplesReport.txt", append = FALSE)
cat("Individuals with doubled samples\n\n")
ID
sink()

## Summarize doubled samples for different SNP chips
Summary <- data.frame(sum(ID\$idbv3), sum(ID\$idbv3_1), sum(ID\$idbv3_2), sum(ID\$idbv3_3), sum(ID\$idbv3_4), sum(ID\$idbv3_5), sum(ID\$idbv3_6), sum(ID\$idbv3_7))

## Write out results
sink(file = "DoubledSamplesReport.txt", append = TRUE)
cat("\n\nSummary by chip\n\n")
Summary
sink()
EOF

chmod +700 DoubledSamples.R

./DoubledSamples.R


## I am using --chr-set 99 instead of --cow all ower the place as there is a chromosome with code 99
## Merge files, calculate genotype call rate and heterozigosity for samples
plink --file idbv3 --merge-list Idbv3FilesToMerge.txt --extract SnpsOnIdbv3.txt --missing --het --cow --make-bed --out Idbv3Merged

## Extrac the individuals that have genotype call less than 0.9
tail -n +2 Idbv3Merged.imiss | awk '$6 >= 0.10 { print $1,$2 }' > IDsWithCallRateLessThan0.90Idbv3Merged.txt

## Draw plot for percentage of genotype call rate and heterozygosity rate for individuals
${S}/DrawHetMissPlotInd.R Idbv3Merged

## Remove individuals with low genotype call rate and heterozygosity rate out of range +-6SD of heterozygosity
# plink --bfile Idbv3Merged --remove IndividualsToExlcudeMissHet.txt --missing --make-bed --cow --out Idbv3MergedCleanInds
mv IndividualsToExlcudeMissHet.txt IndividualsToExlcudeMissHetIdbv3Merged.txt

## Extract SNP with missing information on more than 10% of genotypes
tail -n +2 Idbv3MergedCleanInds.lmiss | awk '$5 > 0.10 { print $1,$2 }' > MarkersWithCallRateLessThan0.90PlinkIdbv3Merged.txt

## Draw plot for percentage of genotype call rate and heterozygosity rate for markers
${S}/DrawMissPlotMarkers.R Idbv3MergedCleanInds

## Remove SNP with missing information on more than 10% of genotypes
plink --bfile Idbv3MergedCleanInds --recode --geno 0.10 --cow --make-bed --out Idbv3MergedCleanIndsMarkers

## Extract only SNP names for the purpose of extraction using plink
cut -d " " -f 2 ../Genotypes/idbv3.map > SnpsOnIdbv3.txt

## Remove SNP with minor allele frequency less than 0.05 and keep only the SNPs that are on the idbv3 SNP chip
plink --bfile Idbv3MergedCleanIndsMarkers --cow --maf 0.01 --make-bed --out FinalIdbv3

# ## Remove intermediate plink files
# for i in ${geno}/*.ped; do
#   a=`echo ${i} | sed -e 's/.*\///' -e 's/.ped//'`
#   rm -f ${a}.* ${a}CleanInds.* ${a}CleanIndsMarkers.*
# done

# rm Idbv3FilesToMerge.txt

# Standard report
echo
pwd
date

