#!/bin/bash

export A=~/Hpc/Illumina/Janez/Analysis
export Q=~/Hpc/Illumina/Janez/Analysis/GenotypesQualityCheck
export S=~/Hpc/Illumina/Janez/Scripts
export pedig=~/Hpc/Illumina/Janez/Pedigree
export ind=~/Hpc/Illumina/Janez/Analysis/SelectedInd

if [ ! -d $A/Chromosomes ];
then
  mkdir $A/Chromosomes
fi

cd $A/Chromosomes

for i in ${ind}/*; do

  a=`echo $i | sed -e 's/.*\///' -e 's/.txt//'`

  if [ ! -d $a ]; then
    mkdir $a
  fi

  cd $a

  ln -sf ${pedig}/${a}/AlphaRecodedPedigree.txt AlphaRecodedPedigree.txt
  ln -sf ${pedig}/Pedigree.txt Pedigree.txt

  rm -f PrepChr.err PrepChr.out

  cp -rf $S/qsub_4_PrepareChromosomes.sh .
  sed -i "s/XXBREEDXX/${a}/g" qsub_4_PrepareChromosomes.sh
  qsub -hold_jid PrepGeno${a} qsub_4_PrepareChromosomes.sh

  cd ..

done
