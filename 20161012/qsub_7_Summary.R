#!/bin/sh

#$ -N SumXXBREEDXX
#$ -cwd
#$ -o Summary.out
#$ -e Summary.err
#$ -l h_rt=2:00:00
#$ -R y
#$ -pe sharedmem 1
#$ -l h_vmem=4G

# Initialise the environment modules
. /etc/profile.d/modules.sh

# Load
module load igmm/apps/R/3.3.0

# Standard report

echo "Working directory:"
pwd
date

./SummarySummary.R

# Standard report
echo
pwd
date
