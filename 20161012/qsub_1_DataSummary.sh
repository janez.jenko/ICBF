#!/bin/sh

#$ -N DataSummary
#$ -cwd
#$ -o DataSummary.out
#$ -e DataSummary.err
#$ -l h_rt=2:00:00
#$ -R y
#$ -pe sharedmem 2
#$ -l h_vmem=8G

# Initialise the environment modules
. /etc/profile.d/modules.sh

# Load
module load igmm/apps/R/3.3.0

# Standard report

echo "Working directory:"
pwd
date

export OMP_NUM_THREADS=8

rm -f DataSummary.err
rm -f DataSummary.out

Rscript -e "require( 'rmarkdown' ); render('1_DataSummary.R', 'html_document')" > DataSummary.log #2>&1 &

# Standard report
echo
pwd
date
