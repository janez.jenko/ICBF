#!/bin/bash

########################################
#                                      #
# Create pedigree on restricted number #
#                                      #
########################################

## Change these locations appropriately
export prog=~/Hpc/Illumina/Janez/Programs
export pedig=~/Hpc/Illumina/Janez/Pedigree
export anal=~/Hpc/Illumina/Janez/Analysis
export ind=~/Hpc/Illumina/Janez/Analysis/SelectedInd

cd ${pedig}

#cut -f 1-3 Pedigree.txt | tail -n +2 > PedigreeForAlphaRecode.txt
#
#ln -sf ${prog}/AlphaSuite/AlphaRecode .
#ln -sf ${prog}/AlphaSuite/AlphaRecodeSpec.txt .
#ln -sf ${prog}/AlphaSuite/qsub_AlphaRecode.sh .
#
#qsub qsub_AlphaRecode.sh
#
#ln -sf ${prog}/PedigreeExtraction/PedExtraction .
#ln -sf ${prog}/PedigreeExtraction/PedExtractionSpec.txt
#ln -sf ${prog}/PedigreeExtraction/qsub_PedExtraction.sh
#
### Select individuals that will be extracted in the pedigree
#cut -d " " -f2 ../Analysis/SelectedInd/PureLmInd.txt > ListId.txt
#
#qsub qsub_PedExtraction.sh

for i in ${ind}/*; do
  a=`echo $i | sed -e 's/.*\///' -e 's/.txt//'`
  if [ ! -d $a ]; then
    mkdir $a
  fi
  cd $a
  ln -sf ${prog}/PedigreeExtraction/PedExtraction
  cp -rf ${prog}/PedigreeExtraction/PedExtractionSpec.txt .
  cp -rf ${prog}/PedigreeExtraction/qsub_PedExtraction.sh .
  sed -i "s/XXBREEDXX/${a}/g" qsub_PedExtraction.sh
  ln -sf ${pedig}/PedReordered.txt
  ln -sf ${pedig}/PedigreeRecoded.txt
  cut -d " " -f2 ${anal}/GenotypesQualityCheck/${a}/LmHdIdbv3CleanIndsMarkers.ped > ListId.txt
#  cut -d " " -f2 ../../Analysis/SelectedInd/${a}.txt > ListId.txt
  qsub -hold_jid PrepGeno${a} qsub_PedExtraction.sh
  ln -sf ${prog}/AlphaSuite/AlphaRecode
  ln -sf ${prog}/AlphaSuite/AlphaRecodeSpec.txt
  cp -rf ${prog}/AlphaSuite/qsub_AlphaRecode.sh .
  sed -i "s/XXBREEDXX/${a}/g" qsub_AlphaRecode.sh
  qsub -hold_jid PedExt${a} qsub_AlphaRecode.sh
  cd ..
done
