#!/bin/bash

########################################
#                                      #
# GE job script for ECDF Cluster       #
#                                      #
########################################


#
## Change these file locations appropriately
export prog=~/Hpc/Illumina/Janez/Programs
export work=~/Hpc/Illumina/Janez
export ind=~/Hpc/Illumina/Janez/Analysis/SelectedInd
export nCHR="29" #Total number of chromosomes
#export nM="47000" # Nr of markers for each chromosome


## Module option was done only for the purpose of Running AlphaPhase for ICBF data
export MODULE=1


if [ "$MODULE" == "1" ]; then
	echo "304 - Run alphaphase"
	cd ${work}/Analysis/Chromosomes
	for i in ${ind}/*; do
	  a=`echo $i | sed -e 's/.*\///' -e 's/.txt//'`
		cd $a
	  for j in `seq 1 1 ${nCHR}` ; do
	  	cd Chromosome${j}
	    if [ ! -d "Phasing" ]; then
        mkdir Phasing
    	fi
      cd Phasing
  	  ln -sf ${prog}/AlphaSuite/alphaphase .
  	  ln -sf ../ch_coded input.gen
  		ln -sf ${work}/Pedigree/${a}/AlphaRecodedPedigree.txt Pedigree.dat

  	  b=`head -n 1 ../ch_coded | cut -d " " -f 2- | awk '{print NF}'`

  	  cat ${prog}/AlphaSuite/AlphaPhaseSpec.txt | sed -e "s/XXNROFSNPXX/${b}/" > AlphaPhaseSpec.txt
      cat ${prog}/AlphaSuite/qsub304.sh | sed -e "s/XXBREEDXX/${a}/" -e "s/XXCHROMXX/${j}/" > qsub304.sh

      rm -f APhase.err APhase.out

	    qsub -hold_jid PrepChr${a} qsub304.sh
  	  sleep 1s
  	  cd ../../
  	done
		cd ../
  done
fi


if [ "$MODULE" != "1" ]; then
	export MAXJOBSPAR="800" # Max nr of jobs running in parallel
	echo "scegli da dove cominciare:
		1. Restart option 1
		2. GeneProb
		3. Restart option 2
		4. Phasing
		5. Restart option 3
		6. GeneProb 2
		7. Restart option 4
		"
	read PROG

	if [ "$PROG" == "1" ]; then

	echo "301 - Run AlphaImpute with restart option =1"
	for j in `seq 1 1 ${nCHR}` ; do

	  cd ${work}/Analysis/Chromosomes/Chromosome${j}/

	  ln -sf ${work}/Pedigree/Pedigree.txt input.ped
	  ln -sf ch${j}_coded input.gen

	  ln -sf ${prog}/AlphaImputev1.5.5-54-g88e236c .
	  ln -sf ${prog}/qsub301.sh .

	  a=`head -n 1 ch${j}_coded | cut -d " " -f 2- | awk '{print NF}'`


	  cat ${prog}/AlphaImputeSpec.txt | sed -e "s/XXNROFSNPXX/${a}/" \
	                          -e "s/XXNRPROCAVXX/20/" \
	                          -e "s/XXRESOPTXX/1/" \
	                          -e "s/XXBYPASSXX/No/" > AlphaImputeSpec1.txt

	  rm -f AlphaSimSpec.txt && ln -sf AlphaImputeSpec1.txt AlphaImputeSpec.txt

	  while [ $(qstat | wc -l) -gt ${MAXJOBSPAR} ]  ; do
	    echo "Queue limit reached (${MAXJOBSPAR} jobs). Waiting ..."
	    sleep 1m
	  done

	  qsub -N S1ch${j} qsub301.sh

	  cd -
	done
	cd ${work}

	elif [ "$PROG" == "2" ]; then

	echo "302 - Run GeneProb"
	echo
	echo

	for j in `seq 1 1 ${nCHR}` ; do
	    cd ${work}/Analysis/Chromosomes/Chromosome${j}/

		for k in GeneProb/*; do
		    if [ -d "${k}" ]; then
		    	echo $k
				cd ${k}
				ln -sf ${prog}/GeneProbForAlphaImpute .
				ln -sf ${prog}/qsub302.sh .

				while [ $(qstat | wc -l) -gt ${MAXJOBSPAR} ]  ; do
						echo "Queue limit reached (${MAXJOBSPAR} jobs). Waiting ..."
						sleep 1m
				done

				G=$(echo $k | cut -c 18-)
				qsub -hold_jid S1ch${j} -N G1ch${j}${G} qsub302.sh
				cd -
				sleep 5s
		    fi
		done
		cd ${work}
	done
	cd ${work}

	elif [ "$PROG" == "3" ]; then


	echo "303 - Run AlphaImpute with restart option =2"
	echo
	echo

	cd ${work}

	for j in `seq 1 1 ${nCHR}` ; do
		cd ${work}/Analysis/Chromosomes/Chromosome${j}/

	  a=`head -n 1 ch${j}_coded | cut -d " " -f 2- | awk '{print NF}'`


		ln -sf ${prog}/AlphaImputev1.5.5-54-g88e236c .
		ln -sf ${prog}/qsub301.sh qsub303.sh

		cat ${prog}/AlphaImputeSpec.txt | sed -e "s/XXNROFSNPXX/${a}/" \
											  -e "s/XXNRPROCAVXX/20/" \
		    								-e "s/XXRESOPTXX/2/" \
		    								-e "s/XXBYPASSXX/No/" > AlphaImputeSpec2.txt

		rm -f AlphaSimSpec.txt && ln -sf AlphaImputeSpec2.txt AlphaImputeSpec.txt

		cat ${prog}/WaitTemplate303.sh | sed -e "s/XXCHRXX/${j}/g" \
										     -e "s/XXMAXJOBSPARXX/${MAXJOBSPAR}/g" > WaitTemplate303.sh

		chmod +x WaitTemplate303.sh

		screen -d -m -S Wait_ch${j} ./WaitTemplate303.sh

		cd -
	done
	cd ${work}

	elif [ "$PROG" == "4" ]; then

	echo "304 - Run alphaphase"

	cd ${work}
	for j in `seq 1 1 ${nCHR}` ; do
	  cd ${work}/Analysis/Chromosomes/Chromosome${j}/

	  for k in Phasing/*; do
	    if [ -d "${k}" ]; then
	      cd ${k}
	      ln -sf ${prog}/alphaphase .
	      ln -sf ${prog}/qsub304.sh .
	      ln -sf ../../input.gen .

	      #while [ $(qstat | wc -l) -gt ${MAXJOBSPAR} ] || [ ! (grep FINISH ../../GeneProb_ch${j}.log ) ] ; do
	        #echo "Queue limit reached (${MAXJOBSPAR} jobs) or GeneProb not finish. Waiting ..."
	        #sleep 1m
	      #done

	      a=`head -n 1 ../../ch${j}_coded | cut -d " " -f 2- | awk '{print NF}'`

	      cat ${prog}/AlphaPhaseSpec.txt | sed -e "s/XXNROFSNPXX/${a}/" > AlphaPhaseSpec1.txt

	      ln -sf AlphaPhaseSpec1.txt AlphaPhaseSpec.txt

	      G=$(echo $k| cut -c 14-)
	##      qsub -hold_jid S2chXXCHRXX -N AP2ch${j}p${G} qsub304.sh
	      qsub -N AP2ch${j}p${G} qsub304.sh
	      cd -
	      sleep 3s
	      fi
	  done
	  cd ${work}
	done

	cd ${work}

	elif [ "$PROG" == "5" ]; then

	echo "305 - Run AlphaImpute with restart option =3"
	echo
	echo
	for j in `seq 1 1 ${nCHR}` ; do
	    	cd ${work}/Analysis/Chromosomes/Chromosome${j}/

			ln -sf ${prog}/AlphaImputev1.5.5-54-g88e236c .
			ln -sf ${prog}/qsub301.sh qsub305.sh

	    a=`head -n 1 ch${j}_coded | cut -d " " -f 2- | awk '{print NF}'`


				cat ${prog}/AlphaImputeSpec.txt | sed -e "s/XXNROFSNPXX/${a}/" \
		    										  -e "s/XXNRPROCAVXX/20/" \
	    		    								  -e "s/XXRESOPTXX/3/" \
	    		    								  -e "s/XXBYPASSXX/No/" > AlphaImputeSpec3.txt

	    		rm -f AlphaSimSpec.txt && ln -sf AlphaImputeSpec3.txt AlphaImputeSpec.txt

	    		cat ${prog}/WaitTemplate305.sh | sed -e "s/XXCHRXX/${j}/g" \
	    										     -e "s/XXMAXJOBSPARXX/${MAXJOBSPAR}/g" > WaitTemplate305.sh

	    		chmod +x WaitTemplate305.sh

	    		screen -d -m -S WaitApR${i}C${j} ./WaitTemplate305.sh

	    		cd -
	    done
	    cd ${work}

	elif [ "$PROG" == "6" ]; then
		echo "305 - Run GeneProb...AGAIN -.-"
	echo
	echo

	for j in `seq 1 1 ${nCHR}` ; do
	    cd ${work}/Analysis/Chromosomes/Chromosome${j}/

		for k in IterateGeneProb/*; do
		    if [ -d "${k}" ]; then
		    	echo $k
				cd ${k}
				ln -sf ${prog}/GeneProbForAlphaImpute .
				ln -sf ${prog}/qsub302.sh .

				while [ $(qstat | wc -l) -gt ${MAXJOBSPAR} ]  ; do
						echo "Queue limit reached (${MAXJOBSPAR} jobs). Waiting ..."
						sleep 1m
				done

				G=$(echo $k | cut -c 18-)
				qsub -hold_jid S3ch${j} -N G2ch${j}${G} qsub302.sh
				cd -
				sleep 5s
		    fi
		done
		cd ${work}
	done
	cd ${work}

	 elif [ "$PROG" == "7" ]; then

	echo "305 - Run AlphaImpute with restart option =4"
	echo
	echo
	for j in `seq 1 1 ${nCHR}` ; do
	    	cd ${work}/Analysis/Chromosomes/Chromosome${j}/

			ln -sf ${prog}/AlphaImputev1.5.5-54-g88e236c .
			ln -sf ${prog}/qsub301.sh qsub305.sh

	    a=`head -n 1 ch${j}_coded | cut -d " " -f 2- | awk '{print NF}'`


				cat ${prog}/AlphaImputeSpec.txt | sed -e "s/XXNROFSNPXX/${a}/" \
		    										  -e "s/XXNRPROCAVXX/20/" \
	    		    								  -e "s/XXRESOPTXX/4/" \
	    		    								  -e "s/XXBYPASSXX/No/" > AlphaImputeSpec4.txt

	    		rm -f AlphaSimSpec.txt && ln -sf AlphaImputeSpec4.txt AlphaImputeSpec.txt

	    		#cat ${prog}/WaitTemplate306.sh | se#d -e "s/XXCHRXX/${j}/g" \
	    										     #-e "s/XXMAXJOBSPARXX/${MAXJOBSPAR}/g" > WaitTemplate306.sh

	    		#chmod +x WaitTemplate306.sh

	    		#screen -d -m -S WaitApR${i}C${j} ./WaitTemplate306.sh
	    		qsub -N S4ch${j} qsub305.sh

	    		cd -
	    done
	    cd ${work}

		fi

fi
