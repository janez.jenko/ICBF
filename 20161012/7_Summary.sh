#!/bin/bash

export S=~/Hpc/Illumina/Janez/Scripts
export work=~/Hpc/Illumina/Janez/Analysis/AlphaSeqOpt
export ind=~/Hpc/Illumina/Janez/Analysis/SelectedInd

cd ${work}

for i in ${ind}/*; do

  a=`echo $i | sed -e 's/.*\///' -e 's/.txt//'`

  cd $a

  ln -sf ${S}/SummarySummary.R
  cp -rf ${S}/qsub_7_Summary.R .
  sed -i "s/XXBREEDXX/${a}/g" qsub_7_Summary.R
  qsub -hold_jid ASeqOpt${a} qsub_7_Summary.R

  cd ..

done
