#!/bin/bash

module load igmm/apps/R/3.3.0

## Here are commands for the preparation of genotypes for the ICBF data

qsub qsub_1_DataSummary.sh

sleep 10

./2_ExtractGenotypes.sh

sleep 10

./3_ExtractPedigree.sh

sleep 10

./4_PrepareChromosomes.sh

sleep 10

#./5_RunAlphaPhase.sh

sleep 10

#./6_RunAlphaSeqOpt.sh

sleep 10

#./7_Summary.sh
