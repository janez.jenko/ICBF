#!/bin/sh

#$ -N PrepChrXXBREEDXX
#$ -cwd
#$ -o PrepChr.out
#$ -e PrepChr.err
#$ -l h_rt=24:00:00
#$ -R y
#$ -pe sharedmem 1
#$ -l h_vmem=8G

# Initialise the environment modules
. /etc/profile.d/modules.sh

# Load
module load roslin/plink/3.36-beta
module load igmm/apps/R/3.3.0
module load intel/2016

# Standard report

echo "Working directory:"
pwd
date

export OMP_NUM_THREADS=8

export A=~/Hpc/Illumina/Janez/Analysis_Purebreds_LmChAaSiHe
export S=~/Hpc/Illumina/Janez/Scripts
export prog=~/Hpc/Illumina/Janez/Programs
export nCHR="29" #Total number of chromosomes

awk '{print $1, $2, $3}' AlphaRecodedPedigree.txt | sort -f -k1 > tmp.AlphaRecodedPedigree.txt
tail -n +2 Pedigree.txt | awk '{print $1, $4}' | sort -f -k1 > tmp.Pedigree.txt
join -i -j 1 -o 1.1,1.2,1.3,2.2 tmp.AlphaRecodedPedigree.txt tmp.Pedigree.txt > PedigreeConflictf90.txt

rm -f tmp.AlphaRecodedPedigree.txt tmp.Pedigree.txt

${S}/ResolveMendelianErrorsPedigree.R

for i in `seq 1 1 ${nCHR}` ; do
  if [ ! -d Chromosome${i} ];
  then
    mkdir Chromosome${i}
  fi

  cd Chromosome${i} || exit

  awk -v var=${i} '$1==var {print $0}' ${A}/GenotypesQualityCheck/XXBREEDXX/AllChipsFinal.map > Markers.map
  awk -v var=${i} '$1==var {print $0}' ${A}/GenotypesQualityCheck/XXBREEDXX/AllChipsFinal.map > Markers.map
  cp ${prog}/genosim/conflict.options.by.chromsome conflict.options
  ln -sf ChromosomeDataConflictf90.txt chromosome.data
  ln -sf ../PedigreeConflictf90.txt pedigree.file
  ln -sf GenotypesConflictf90.txt genotypes.raw
  ln -sf ${prog}/genosim/conflict .

  ## This will produce 29 chromosomes coded into 0 1 and 2 using recode option of plink

  plink --file ${A}/GenotypesQualityCheck/XXBREEDXX/AllChipsFinal --cow --recode A --chr ${i} --out chr
  rm chr.log chr.nosex

  ${S}/ResolveMendelianErrorsGenotypes.R

  ## This will produce 29 files called chx_coded (where x is the number of the chromosome) ready for AlphaImpute/Aplhaphase.
  ## The input files are the .raw files from the previous loop

#  tail -n +2 chr.raw > file_flipped
#  cut -d " " -f 7- file_flipped > SNPs
#  cut -d " " -f 2 file_flipped > ID
#  sed -i "s/\bNA\b/9/g" SNPs
#  paste -d ' ' ID SNPs > ch_coded
#  rm ID SNPs file_flipped

  cd ..

done

# Standard report
echo
pwd
date
