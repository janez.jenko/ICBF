#!/bin/bash

## Create working directory for AlphaSeqOpt

export A=~/Hpc/Illumina/Janez/Analysis_Purebreds_LmChAaSiHe
export prog=~/Hpc/Illumina/Janez/Programs
export work=~/Hpc/Illumina/Janez
export fCHR="1" #From chromosome
export tCHR="29" #To chromosome
export HapExcl="1 2 3 4 5 6 11 0.01Perc 0.05Perc 0.5Perc 5Perc"
export Perc="0 0 0 0 0 0 0 1 1 1 1" #This is done because for scenarios with percentage treshold we substitute treshold restriction as s/XXHAPLOTHRESXX/0/g
export seq="0" #This is parameter is only created to select appropriate element from array. It is updated within a loop

## Load module (this is required for AlphaSeqOpt)
module load intel/2016
module load igmm/apps/R/3.3.0

## This was used only once to create cost file that needs to be changed only if IndAlreadySequenced.txt is going to be changed
## ./${work}/Scripts/CostsForAlreadySequencedAnimals.R

cd ${A} || exit

for i in ${A}/SelectedInd/*; do

  a=`echo ${i} | sed -e 's/.*\///' -e 's/.txt//'`

  ## Prepare things for AlphaSeqOpt OptimisationMethod Coverage
  cd AlphaSeqOpt/${a} || exit

  for j in ${HapExcl}; do
    seq=$(($seq+1))

    cd ${j} || exit

    ln -sf ${A}/Pedigree/${a}/PedigAlphaSeqOpt_Costs.txt Pedigree.txt
    ln -sf ${A}/Pedigree/${a}/NotGenotypedParents.txt .
    ln -sf ${A}/Pedigree/${a}/PriorSeqCosts.txt .
    ln -sf ${work}/PreSequenceInfo/IndAlreadySequenced.txt .
    ln -sf ${prog}/AlphaSuite/AlphaSeqOpt .
    ln -sf ${prog}/AlphaSuite/Metrics.txt .
    ln -sf ${prog}/AlphaSuite/Scenarios11.txt .
    ln -sf ${prog}/cpumemlog/cpumemlog.sh
    ln -sf ${prog}/cpumemlog/cpumemlogplot.R .

    b=`wc -l Pedigree.txt | cut -d" " -f1`

    c=`echo ${Perc} | cut -d" " -f$seq`

    d=`wc -l SequencedInd.txt | cut -d" " -f1`

    e=`wc -l IndToSequenceToReach80PercCoverage.txt | cut -d" " -f1`

    f=$((${e}-${d}))

    ## Calculation of sequencing costs Nanim*LibabrCosts+Nanim*Coverage*CoverageCosts
    if [[ ${f} -gt 0 ]]; then
      g=$(((${f})*40+(${f})*10*100))
    else
      g=0
    fi

    cat ${prog}/AlphaSuite/AlphaSeqOptSpec_Costs.txt | sed -e "s/XXTCHRXX/${tCHR}/g" \
                                                           -e "s/XXNROFINDXX/${b}/g" \
                                                           -e "s/XXNFAMSEQXX/${e}/g" \
                                                           -e "s/XXCOSTSXX/${g}/g" > AlphaSeqOptSpec_Costs.txt

    if [[ ${c} -eq 0 ]]; then
      sed -i "s/XXHAPLOTHRESXX/${j}/g" AlphaSeqOptSpec_Costs.txt
    fi

    if [[ ${c} -eq 1 ]]; then
      sed -i "s/XXHAPLOTHRESXX/0/g" AlphaSeqOptSpec_Costs.txt
    fi

    cd Cores || exit

    for k in `seq ${fCHR} 1 ${tCHR}`; do

      h=`head -1 Chromosome${k}.txt | awk '{print NF}'`
      h=$(((${h}-1)/2))

      sed -i "s/XXNROFCORESCHR${k}XX/${h}/" ../AlphaSeqOptSpec_Costs.txt

    done

    cd ..

    ln -sf AlphaSeqOptSpec_Costs.txt AlphaSeqOptSpec.txt

    cat ${work}/Scripts/qsub_AlphaSeqOpt_Costs.sh | sed -e "s/XXBREEDXX/${a}/g" \
                                                        -e "s/XXHAPLOTHRESXX/${j}/g" > qsub_AlphaSeqOpt_Costs.sh

    rm -f ASeqOptCosts.err ASeqOptCosts.out

    if [[ ${c} -eq 0 ]]; then
      qsub -hold_jid ASeqOptPedig${a},ASeqOpt${a}Hap${j} qsub_AlphaSeqOpt_Costs.sh
    fi

    if [[ ${c} -eq 1 ]]; then
      qsub -hold_jid ASeqOptPedig${a},ASeqOpt${a}Hap0 qsub_AlphaSeqOpt_Costs.sh
    fi

    cd ..

  done

  seq="0"

  cd ../../ || exit

done
